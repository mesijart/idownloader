// оголошуємо усі основні змінні

var next_id = 0;                // ід наступного файлу
var all = 0;                    // загальна кількість зафантажених файлів
var all_size = 0;               // загальна кількість файлів
var failed = 0;                 // кількість фейлових файлів
var failed_size = 0;            // загальна кількість фейлових файлів
var failed_size_dw = 0;         // загальна кількість фейлових файлів яку потрібно завантажити
var copied_size_dw = 0;         // загальна кількість попередньо завантажених файлів яку потрібно завантажити
var copied = 0;                 // загальна кількість завантажених файлів
var copied_size = 0;            // загальна кількість попередньо завантажених файлів
var d = [];                     // масив з данними які отримали з файлу
var size = 0;                   // загальний розмір усіх отриманих данних
var step = 0;                   // крок відкриття файлу
var step_id = 0;                // іп стрічки в кроці
var dir = '';                   // папка в яку будемо завантажувати файли
var migration = [];             // штформація про міграцію
var migration_status = false;   // статус міграції
var active = true;              // статус завантаження
var process_info = 0;           // кількість процесів яку потрібно запустити
var alertSize = 0;              // кількість активних алертів
var newDir = '';                // тимчасова змінна для імені папки
var defaultDir = '';            // оголошення змінної для дефолтної папки завантаження файлів
var allFileSize = 0;            // загальний розмір завантажених файлів
var colorIntSize = 7;           // довжина каунтів на гріді завантаження
var completeTargetImage = [];   // кеш усіх скачсаних картинок

// параметри завантаження

var param_other_ext = false;    // шукати файли схожих розширень
var param_using_proxy = false;  // використовувати проксі
var param_presta_img = false;   // завантажувати тільки оригінальні зображення ( виключно для міграції на престу )
var param_convert_img = false;  // конвертувати картинки в jpg
var param_only_failed = false;  // завантажувати тільки фейлові

// оголошуємо константи

var downloadFolder =  dif_downloadFolder;     // папка в яку виконуються усі завантаження
var pack =            dif_pack;               // кількість рядків в кроці при відкритті файлу
var proces =          dif_proces;             // дефолтна кількість процесів
var lockOn =          dif_lock;               // блокування випадкового перезавантаження сторінки
var theme_array =     parse(dif_theme_array); // масив доступних тем
var theme =           dif_theme;              // активна тема

setTimeout(unBug(false),0);
function unBug(test){
  if(test){
    actionShow();           // виділяє кольором текст в блоці
    add();                  // додає вказану кількість процесів
    alert();                // виводить повідомлення
    alertHide();            // приховує повідомлення
    alertDrop();            // видаляє блок повідомлення
    check();                // активує/дективує кнопку Only failed
    changeTheme();          // змінює тему
    closeEditorWarning();   // підтвердження закриття вклідки
    createLoader();         // створює індикатор завантаження
    deleteDir();            // видаляє папку
    deleteFile();           // видаляє файл
    download();             // запускає процеси завантаження
    lock();                 // блокує випадкове перезавантаження сторінки
    finish();               // виконує необхідні функції при завершенні завантаженя
    next();                 // вертає id наступного файлу для завантаження
    openFile();             // завантажує данні з файлу
    parse();                // парсимо відповідь від серверу
    perDir();               // надає права 777 для папки рекурсивно
    process();              // процес завантаження файлу
    renameDir();            // перейменовує папку
    renameFile();           // перейменовує файл
    res();                  // перезавантажує сторінку
    start();                // підготовує сторінку до завантаження файлів
    stat();                 // оновляє статистику на сторінці
    view();                 // виводить відповідь від серверу
  }
}

$(function() {
  $(".knob").knob({});
});

/**
 * @param block
 * @param time
 * @param step
 * @returns {boolean}
 */
function actionShow(block, time, step){
  if(time){
    if(step){
      $(block).animate({opacity:0}, 500);
      step = 0;
    }
    else{
      $(block).animate({opacity:1}, 500);
      step = 1;
    }
    setTimeout(function(){actionShow(block, --time, step);}, 500);
  }
  else
    return true;
}

/**
 * @param count
 * @param forse
 * @returns {*}
 */
function add(count, forse){
  if(active){
    var addProcess = download(count, forse);
    if (addProcess)
      alert('Add ' + count + ' process','ok');
    else
      alert('Error add process','error');
    if(process_info >= 50){
      $('#addProcessButton').animate({opacity:0}, 500).delay(500).css({display:'none'});
    }
    return process_info;
  }
  return false;
}

/**
 * @param text
 * @param type
 * @returns {number}
 */
function alert(text,type){
  var id = Math.floor((Math.random() * 1000) + 1);
  if (type === 'ok') {
    $("#alertBox").append('<div class="alertBox alertID-' + id + '">' +
      '<div id="alert" class="' + type + '">' +
      '<div class="icon "><span id="alertIcon" class="icon-checkmark-circle"></span></div>' +
      '<div class="text" id="alertText"></div>' +
      '</div></div>');
  } else if (type === 'error') {
    $("#alertBox").append('<div class="alertBox alertID-' + id + '">' +
      '<div id="alert" class="' + type + '">' +
      '<div class="icon "><span id="alertIcon" class="icon-spam"></span></div>' +
      '<div class="text" id="alertText"></div>' +
      '</div></div>');
  } else {
    $("#alertBox").append('<div class="alertBox alertID-' + id + '">' +
      '<div id="alert" class="info">' +
      '<div class="icon "><span id="alertIcon" class="icon-info2"></span></div>' +
      '<div class="text" id="alertText"></div>' +
      '</div></div>');
  }
  $(".alertID-" + id + " #alertText").html(text);
  $(".alertID-" + id).animate({opacity: 1, marginTop: (275 + (alertSize * 60)) + 'px'}, 1000);
  alertSize++;
  setTimeout(function(){alertHide(id);}, 2500);
  return id;
}

/**
 * @param id
 * @returns {boolean}
 */
function alertHide(id){
  $(".alertID-"+id).animate({opacity: 0, marginTop: 0}, 1000);
  setTimeout(function(){alertDrop(id);},1000);
  alertSize--;
  return true;
}

/**
 * @param id
 * @returns {boolean}
 */
function alertDrop(id){
  $(".alertID-" + id).remove();
  return true;
}

/**
 * @returns {boolean}
 */
function check(){
  if(!active){
    if($(".only button").hasClass("ok")){
      $(".only div button").removeClass("ok");
      $("#only_failed").text("Download all");
    }
    else{
      $(".only div button").addClass("ok");
      $("#only_failed").text("Only failed");
    }
    return true;
  }
  return false;
}

function changeTheme(){
  theme_array.remByVal(theme);
  var theme_id = Math.floor((Math.random() * theme_array.length));
  $.get("index.php?theme=" + theme_array[theme_id], function( data ) {
    data = parse(data);
    if(data['code'] == 200){
      res(0, 1, 0);
    }
    view(data);
    return data['code'] == 200;
  });
}

/**
 * @returns {string}
 */
function closeEditorWarning(){
  return 'Are you sure?\n\nDownload process been closed!'
}

function closeUpdateBox() {
  $('#updateBox').animate({opacity:0}, 500, function(){
    $('#updateBox').hide();
  });
  $('#updateContent').animate({marginTop:'3%'}, 500);
}

function closeUpdateProcessBox() {
  $('#updateProcessBox').animate({opacity:0}, 500, function(){
    $('#updateProcessBox').hide();
  });
  $('#updateProcessContent').animate({marginTop:'3%'}, 500);
}

/**
 * @param dir
 * @returns {boolean}
 */
function deleteDir(dir){
  if(confirm("Delete dir " + dir + "?")){
    $.get("index.php?deleteDir=" + dir, function( data ) {
      data = parse(data);
      if(data['code'] == 200)
        res(0, 0, 0);
      view(data);
      return data['code'] == 200;
    });
  }
  else
    return false;
}

/**
 * @param file
 * @returns {boolean}
 */
function deleteFile(file){
  if(confirm("Delete file " + file + "?")){
    $.get("index.php?deleteFile=" + file, function( data ) {
      data = parse(data);
      if(data['code'] == 200)
        res(0, 0, 0);
      view(data);
      return data['code'] == 200;
    });
  }
  else
    return false;
}

/**
 * @param count
 * @param forse
 * @returns {boolean}
 */
function download(count,forse){
  if((process_info != 50) || forse){
    if(process_info == 40 || count == 50){
      $(".process button").hide();
      $(".process .left").addClass('center red').removeClass('left');
    }
    var i = 0;
    while(count > i){
      setTimeout(process(false, false), 0);
      process_info++;
      i++;
    }
    return true;
  } else {
    $(".process button").hide();
    $(".process .left").addClass('center red').removeClass('left');
    return false;
  }
}

/**
 * @returns {boolean}
 */
function lock(){
  if (lockOn == 1){
    $("#lock").addClass("lock-off");
    lockOn = 0;
  } else {
    $("#lock").removeClass("lock-off");
    lockOn = 1;
  }
  $.get("index.php?lock=" + lockOn, function( data ) {
    data = parse(data);
    view(data);
    return data['code'] == 200;
  });
}

/**
 * @returns {boolean}
 */
function finish(){
  if(!size)
    return false;
  stat();
  window.onbeforeunload = '';
  active = false;
  $('#addProcessButton').animate({opacity:0}, 500).delay(500).css({display:'none'});
  if(proces == 0){
    $.get("index.php?finish=" + dir, function( data ) {
      data = parse(data);
      if(data['code'] == 200){
        document.title = "Finish download " + migration['id'];
      }
      else{
        document.title = "Error finish download " + migration['id'];
      }
      view(data);
      return data['code'] == 200;
    });
  }
  else
    return false;
}

/**
 * @returns {*[]}
 */
function next(){
  var st = step;
  step_id++;
  if(step_id == pack) {
    step_id = 0;
    step++;
  }
  var id = next_id;
  next_id++;
  if(next_id > size)
    id = -1;
  return [id, st];
}

function colorInt (intValue, length){
  if(intValue.toString().length < length){
    var add = length - intValue.toString().length;
    var prefix = '';
    while (add > 0) {
      prefix += '0';
      add--;
    }
    return '<span>' + prefix + '</span>' + intValue;
  }
}

function getFileInfo (file,type){
  $('#updateProcessBox').show().animate({opacity:1}, 500);
  $('#updateProcessContent').css({height:'120px'}).animate({marginTop:'5%'}, 500);
  $('#updateProcessText').text('Load file: ' + file);
  defaultDir = file.replace('.csv', '');
  active = false;
  $('#updateLoadAnimate').html('' +
    '<input type="submit" value="Open" onclick="openFile(\'' + file + '\', 0, ' + type + ', 1)"/>' +
    '<input type="text" autofocus maxlength="15" id="inputOpenFileDirName" value="' + defaultDir + '" />'
  );

  return '';
}

function getDirNameFromInput(){
  var id = $('#inputOpenFileDirName').val().trim();
  migration['id'] = id;
  migration['target_store_id'] = 0;
  return id;
}

/**
 *
 * @param file
 * @param part
 * @param type
 * @param getLocalName
 * @returns {boolean}
 */
function openFile(file, part, type, getLocalName){

  if(dir == ''){
    if(getLocalName == 1){
      dir = getDirNameFromInput();
    } else {
      dir = getFileInfo(file, type);
    }
    if(dir == ''){
      return false;
    }
  }

  if (part == 1) {
    placeholder('add');
  }
  $.get("index.php?loadFile=" + file + "&step=" + part + "&type=" + type, function( data ) {
    data = parse(data);
    if(data['data'] != '[]'){
      if(data['code'] == 400 || data['code'] == 404){
        view(data);
        res(0, 0, 0);
        return false;
      }
      else {
        d[part] = parse(data['data']);
        part++;
        var str = '' + (part * dif_pack);
        alert('Open file row ' + str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '), 'info');
        openFile(file, part, type, 0);
        return true;
      }
    }
    else {
      var mass = d.length;
      var i = 0;
      while(mass){
        mass--;
        var img = 0;
        while(img != pack){
          if(window.d[i][size]){
          if(d[i][size][0] == 1)
            copied_size++;
          else
            failed_size++;
          size++;
          }
          img++;
        }
        i++;
      }

      closeUpdateProcessBox();
      showDownloadPanel(file);
      placeholder('remove');
      alert('Open file ' + file, 'ok');
    }
  });

  return true;
}

function showDownloadPanel (file){

  $('.fileList').hide(0);

  $(".infoGridSourceFile").html('<a href="' + csvFolder + '/' + file + '" target="_blank">' +
    '<span>' + csvFolder + '/</span>' + file + '</a></div>');
  $(".infoGridFailedImages").html('<a href="' + downloadFolder + '/' + file.replace('.csv', '') + '/' + file +
    '" target="_blank">' + '<span>' + downloadFolder + '/' + dir + '/</span>' + file + '</a></div>');
  $(".infoGridMigrationId").html(migration['id']);

  var i = 70;
  $('.dwGridItem').each(function(){
    $(this).delay(500 + i).transition({scale:1,opacity:1,marginTop:0}, 300);
    i += 70;
  });

  setTimeout(function(){
    i = 70;
    $('.circleLoader').animate({opacity:1}, 1000);
    $('.circleLoaderItem').each(function(){
      $(this).transition({rotate:(i / 1.5) + 'deg', scale:1.5}, 0).delay(i).transition({rotate:'0deg', scale:1}, 1500);
      i += 70;
    });
  }, 500);

  setTimeout(function(){
    i = 70;
    $('.downloadSettItem').each(function(){
      $(this).delay(500 + i).animate({opacity:1, marginTop:0}, 300);
      i += 70;
    });
  }, 1000);

  setTimeout(function(){
    $('#startButton').animate({opacity:1}, 500)
  }, 1500);

  $(".countOpenAll").html(colorInt(size, colorIntSize));
  $(".countOpenCopied").html(colorInt(copied_size, colorIntSize));
  $(".countOpenFailed").html(colorInt(failed_size, colorIntSize));
  $(".countDownloadAll").html(colorInt(all, colorIntSize));
  $(".countDownloadFailed").html(colorInt(failed, colorIntSize));
  $(".countDownloadCopied").html(colorInt(copied, colorIntSize));
  $("#downloadBox").show(0);
}

/**
 * @param data
 * @returns {*}
 */
function parse(data){
  return $.parseJSON(data);
}

/**
 * @param dir
 * @returns {boolean}
 */
function perDir(dir){
  $.get( "index.php?perDir=" + dir, function( data ) {
    data = parse(data);
    if(data['code'] == 200){
      view(data);
      res(0, 0, 0);
      return true;
    }
    else{
      view(data);
      return false;
    }
  });
  return false;
}

/**
 * @returns {boolean}
 */
function process(id, step){
  if (!id && !step){
    var mass = next();
    id = mass[0];
    step = mass[1];
  }

  if(id != -1){
    if((param_only_failed) && (d[step][id][0] == 1)){
      process(false, false);
      return true;
    }
    else {
      if (completeTargetImage[d[step][id][2]] != undefined) {
        if (completeTargetImage[d[step][id][2]] == 'wait') {
          setTimeout(function(){process(id, step);}, 1000);
          return false;
        }
        copied++;
        if(d[step][id][0] == 0){
          failed_size--;
        } else {
          copied_size--;
        }
        all_size--;
        all++;
        stat();
        process(false, false);
        return true;
      } else  {
        completeTargetImage[d[step][id][2]] = 'wait';
      }
      var param = {};
      param.otherExt = param_other_ext;
      param.usingProxy = param_using_proxy;
      param.prestaImg = param_presta_img;
      param.convertImg = param_convert_img;
      $.post(
        "index.php",
        {
         ts: migration['target_store_id'],
      param: JSON.stringify(param),
     action: 'download-file',
          s: d[step][id][1],
          t: d[step][id][2],
        dir: dir
        },
      function( data ) {
        data = parse(data);
        allFileSize += data['data'];
        if(data['code'] == 200) {
          copied++;
          completeTargetImage[d[step][id][2]] = data['data'];
        } else {
          if (completeTargetImage[d[step][id][2]] != undefined) {
            completeTargetImage[d[step][id][2]] = undefined;
          }
          failed++;
        }
        if(d[step][id][0] == 0){
          failed_size--;
        } else {
          copied_size--;
        }
        all_size--;
        all++;
        stat();
        process(false, false);
        return data['code'] == 200;
      });
    }
  }
  else {
    proces--;
    process_info--;
    finish();
    return false;
  }
}

/**
 * @returns {boolean}
 */
function createLoader(){
  $(function() {
    loader = $("#topLoader").percentageLoader({width: 256, height: 256, controllable : false, progress : 0, onProgressUpdate : function(val) {
      loader.setValue(Math.round(val * 100.0));
    }});
    $(".topLoader").click(function() {
      start();
      return true;
    });
    var topLoaderRunning = false;
    $("#starts").click(function() {
      if (topLoaderRunning) {
        return false;
      }
      topLoaderRunning = true;
      loader.setProgress(0);
      loader.setValue('0');
      var kb = 0;
      var totalKb = 100;
      var animateFunc = function() {
        kb += 17;
        loader.setProgress(kb / totalKb);
        loader.setValue(kb.toString() + '%');
        if (kb < totalKb) {
          setTimeout(animateFunc, 25);
        } else {
          topLoaderRunning = false;
        }
      };
      setTimeout(animateFunc, 25);
    });
  });
  return true;
}

/**
 * @param dir
 * @returns {boolean}
 */
function renameDir(dir){
  var name = prompt('Enter new name for folder ' + dir).trim().replace(/[^a-zA-Z0-9а-яА-ЯіІїЇєЄ_]/, '_');
  if(name != ''){
    $.get( "index.php?renameDir=" + dir + "&name=" + name, function( data ) {
      data = parse(data);
      if(data['code'] == 200)
        res(0, 0, 0);
      view(data);
      return data['code'] == 200;
    });
  }
  else
    return false;
}

/**
 * @param file
 * @returns {boolean}
 */
function renameFile(file){
  var name = prompt("Enter new name for file " + file + "\n\nSet empty for auto rename file");
  if(name !== null){
    $.get( "index.php?renameFile=" + file + "&name=" + name.replace(/[^a-zA-Z0-9а-яА-ЯіІїЇєЄ_\.]/, '_'), function( data ) {
      data = parse(data);
      if(data['code'] == 200){
        view(data);
        res(0, 0, '.file-' + data['data']['name']);
        return true;
      }
      else if(data['code'] == 201){
        view(data);
        return true;
      }
      else {
        view(data);
        return false;
      }
    });
  }
  else
    return false;
}

/**
 * @param send
 * @param forse
 * @param action
 * @returns {boolean}
 */
function res(send,forse,action){
  if(size || forse){
    window.location = './';
    return false;
  }
  else{
    $.get("index.php?getContent=1", function( content ) {
      if(content){
        if(send)
          alert('Reload page', 'ok');
        $("#content").html(content);
        if(action){
          $(action).addClass('action');
          actionShow('.action', 10, 1);
        }
        document.title = "IDownloader";
        $(".knob").knob({});
        return true;
      }
      else{
        alert('Error reload page', 'error');
        return false;
      }
    });
  }
}

/**
 * @returns {boolean}
 */
function start(){
  if (active) {
    return false;
  }
  if (lockOn == 1) {
    window.onbeforeunload = closeEditorWarning;
  }
  all_size = size;
  failed_size_dw = failed_size;
  copied_size_dw = copied_size;
  $.get("index.php?start="+dir, function( data ) {
    data = parse(data);
    if(data['code'] == 200){
      active = true;
      $('#addProcessButton').css({display:'inline-block'}).animate({opacity:1}, 500);
      param_other_ext = $('input[name=FAST_EXTENSIONS]').is(':checked') ? true : false;
      param_using_proxy = $('input[name=FAST_PROXY_ACTIVE]').is(':checked') ? true : false;
      param_presta_img = $('input[name=FAST_PRESTA]').is(':checked') ? true : false;
      param_convert_img = $('input[name=CONVERT_IMAGES]').is(':checked') ? true : false;
      param_only_failed = $('input[name=FAST_ONLY_FAILED]').is(':checked') ? true : false;
      $('.downloadSettItem input[type=checkbox]').prop('disabled', true);
      $('.downloadSettItem').addClass('disabled');
      view(data);
      add(proces,false);
      return data['code'] == 200;
    }
  });
  return false;
}

/**
 * @returns {boolean}
 */
function stat(){
  var perAll = 0;
  var perCopied = 0;
  var perFailed = 0;
  if(param_only_failed){
    perAll = parseInt(((all / failed_size_dw) * 100).toString());
    perCopied = parseInt(((copied / failed_size_dw) * 100).toString());
    perFailed = parseInt(((failed / failed_size_dw) * 100).toString());
  } else {
    perAll = parseInt(((all / size) * 100).toString());
    perCopied = parseInt(((copied / size) * 100).toString());
    perFailed = parseInt(((failed / size) * 100).toString());
  }

  document.title = "Download "+perAll+'%';
  $('#startButton').text(perAll+'%');
  var processColor = '#ffffff';
  if(process_info == 0){
    processColor = '#aaaaaa';
  }
  $('.barProcess').text(process_info).css({width:(process_info * 2) + '%', color:processColor});
  $('.infoGridImagesSize').text(parseInt((allFileSize / (1024000)).toString()));

  $(".circleAll").val(perAll).trigger("change");
  $(".circleFailed").val(perFailed).trigger("change");
  $(".circleCopied").val(perCopied).trigger("change");

  $(".countOpenAll").html(colorInt(all_size, colorIntSize));
  $(".countOpenCopied").html(colorInt(copied_size, colorIntSize));
  $(".countOpenFailed").html(colorInt(failed_size, colorIntSize));
  $(".countDownloadAll").html(colorInt(all, colorIntSize));
  $(".countDownloadFailed").html(colorInt(failed, colorIntSize));
  $(".countDownloadCopied").html(colorInt(copied, colorIntSize));

  return true;
}

/**
 * @param data
 * @returns {boolean}
 */
function view(data){
  if(data['data'] instanceof Object)
    alert(data['data']['message'],data['type']);
  else
    alert(data['data'],data['type']);
  return true;
}


Array.prototype.remByVal = function(val) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] === val) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

function settingsOpen(){
  $('#settingsBox').show().animate({opacity:1}, 500);
  $('#settingsContent').animate({marginTop:'5%'}, 500);
}

function addFile(id){
  $('#updateProcessBox').show().animate({opacity:1}, 500);
  $('#updateProcessContent').animate({marginTop:'5%'}, 500).css({height:'120px'});
  $('#updateProcessText').text('Load file');
  $('#updateLoadAnimate').html('' +
    '<input type="text" style="width: 230px" autofocus maxlength="15" id="inputUploadFileName" placeholder="ID" value="'+id+'" />'+
    '<input type="submit" value="Full" onclick="loadFile(\'full\')" style="margin-left: 10px;"/>'+
    '<input type="submit" value="Demo" onclick="loadFile(\'demo\')"/>'
  );
}

function placeholder(action) {
  if (action == 'add') {
    $('#updateProcessBox').append('<div id="updateProcessBox" class="addFilePreload" style="display: block; opacity: 1;"><div id="updateProcessContent" style="margin-top: 5%; height: 120px;">' +
      '<div style="height:100px; width:100px; margin:15px auto 0 auto; background: url(\'data:image/gif;base64,R0lGODlhZABkAKUAAExKTIyOjGxubLSytFxeXKSipHx+fMTCxFRWVJyanHR2dLy6vGRmZKyqrISGhMzKzFRSVJSWlExOTJSSlHRydLS2tGRiZKSmpISChMTGxFxaXJyenHx6fLy+vGxqbKyurIyKjMzOzEdHRwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQIBQAAACwAAAAAZABkAAAG/kCRcEgsGo/IpHLJbDqfUABEAjVKIICqdvsESASTiyDLZVwiAgSZy+Z6GZPKIRPirLWCzOOwiHiwbYFNAAQGDQ+IIQ91d1UCiIuKHxgWVIKXRAAaAR96kYsPdmwedKCgAxMEjZhcEBidiiGxkaJcebOKixkLARqsbhIUFXS4oIq1W4/Gn7kHHGq/XQwJxMW4yHh61p8ZBWPRTAAcA6XL1tiOpduzFQ4Q4EgACBMdsszmoOhQyuvGId0aVkUDwKCBvX7XBDrJg2/bogHf4IkAIGBALoTL9D0hhTFWrA4cLA1UUMFjw2UPGjhA0AaBgQuJTs5a1AGDSEwSDByQibJD/gIGEhRGAWAhTsx1uTJMuBkIgM6OixZMsCCUjSYHFY42zBWAqRsFOzseSOBhFQQGFLzG8yAAUCYGAeqdNJYBhNooAkryVLSg7RoAhLB2uMCyiQR6FaYGHeLFwwetJj8aqLqEoMW9ByYUFiJFAMxYDTYvOTzrw7M7EkB0gNxTAeUkCAzuXWDgJgQFF4iBCu1EQoRlGRoYEA1AwWOEIQZY2AJgQscFZRkTKFCvGGEnEJz7C3GgQXTOBGQjvPAuCge5/S5Q5SwBwwJTuHgb1j5TVodeaxBsYD0rQ1dpevVTgCrg7dePfEz4Zk58DOT3G08LUPCaEBIkwB8o6pFB0AcH/jXUQHkJ/tbPAwMoIJJ+MimC4BIUaHPSAutNVJE6+GSwQUThTHMAUg/UtIYGhzj0DwZNQBBgQxV8V1x121TQSysYkCNkAGuEt5UuBFSGAY3FHDAZZwJUhw8vMbqBAAhZOYSBhmGOGMFdQmjAYT9vcsZASds0IACcVQDgQQFcRgISGe3xJwt0SQAQwIUhVFAYIY/hkxk0mUhAQJlFEFJJI144EJY5SRL6GT6IRABiEQQc19ABFAwBQQENgcSUJgoAusGpmSx6ASWcevCeNR/4IgQDq1lj33eZcMBoAuU5FWgIC6ADgQGwhMBsPM79MwB+mVRUzCIRaAjCs4lQaUWQ/kh+R8AC1gz6Vm4HXXuEov3peRNFR9pzgABDEDDAlQuINuyFDyxFIaxdYiOBp8XIawS9HiliarfFLhMsZwa4eE6mAfTTwXITKYCeMRPcMU+XF4QUDwXU5ZJIAgExlrE5GThABgSX5XmHkTIlQAYCBXSoomgIREBjSqcp4YUCG+j2TwHCCgFBAt8OICwAGHic5RAe7HgSA5y1WEwHOB5GY02UhnPbe6DYuBlR/+KSwZpCIOC1NXNrOAG5ITRgCQAXFKOUSAA4MNMAYG+hAbz2+CyzxlE1K2JDBVgCQeAn0S0CAZAtsPWwdz9wQYNsaEBNTK65Kp4xrU7U4kkDbB0M/gcG0G577cMNsWgx5ko9anKYaiEPwoh88Hlx+BRAqAK43047BW8DJv30a0iQJl1ROwtKB6QLgsCvEoskwa/GeM4Y9dRr4UHFsSRARKqmEMmKAJ8ukLgQhi/j5YR9etbA/wDkF2c44KIVCaJCxgDBGixwAQA24AO74l8VJICAClowbRIg3gFSxwqCVKwCIJLHBSu4GIkogQDquE40IDC5EHjAhIGwQAI2MEONNEUACZhhAgQIQzdA4IduAYcXrjCFHhrxiEhMohKXyMQmOvGJUIyiFKdIxSpa8YpuSB880IdFIRBgAgGIQAAkBA+4hDEAPLyiBQ5yKyG2UAFOHKIE/uY4x+qJpwNk/AUDctaBt13hj1Pgk1U8MIEIFPKQ93OKMRx2CUWpo2RD0EAEEjBJSk6Ag9Fo0zIiQAQLxO0BGWgdJoj1ERwZ4B55g8f3zHGAEAZAHQHDxOVM4TAAcOgoH5sgAwTAy176slkTwIfmRCCnWVwgZm3IjjGSRAQPRCYllpAAW3zpyz/UDSZ6yCYo91ALBuBjAG+jQEwyEIGobSE1I7PZEDKIDw6AaTWe0GYGPgAyADwIEvgMQeUo9MlY1OwvEehPAQT2hOzsKBbkYYzIuvSoRZkinwkVAgUOKouK6qKeCtAKibonAhSZogJ+kcbvUvI5YqJrFgqUGrtw/mGPB6izbmzLJygg2VFVeaSN4AGURzLjAUGKQAIWiAtdPoAsCTgUF/YbggKEdACQcYZqFS1GBa42s5mkMqfweUAFdqgQghjFYsgSAf3w8Z+f3nIbH2CKMiDzj5R2dHX2INtfWLidB9RpXg5dxgUI1C/yzQJxnAGLTORHBJwJzRQfuF/XxoSjn3JAVYtgZCaC2dJ2nIoQ/6JZLWIjkwOYkzMYMNQDytoctDaWEBFgV2RxxZiO0eQnTLFAA5yGob895SR3LcKdOns/IBkrVEWQAFzes4G7lLYDE2iLEeBnDRhFMmeSymNw9yYkv9kpK/jogAFwBRgI8NK4AoAeYIIr/gDwLcNdE2khPnB6BAvk6x4O+JtgrTGW4FllHiPrT3wDyxM8CqU5BPMvhQzQjw9sV4JIkIACZruOADTLAmclVXGXgADIFaMBnzPqlQ5wARO5wQMbCMs2EnAiA50kAy9kQsYQgqD2fIqlimiHfePhkjmd2MGuohpPekfhkVpjA2/jwHsvgogCsDbB9DnJAdyxTsrKJHZOoIhfaxSBt1VkK7IwYBIU1I8FeJhCr+zvl6LsAL417m2StDAktJxg9fYnQ67aG08eIFkmvGpEi8Bw9SgQKZay+QhcRskAtssYBFgIIx8gaBP8xSNFiOEOBHBAZmfxZytYaBlSKRMhNtCR/g6kWHgi20uPbJKJSLOv0plK8gFAAJRMKGDSnR2m8HQi6gyo514QoMAFFsBhRR8hO7toQNKkY7SO1MWnoykzVBbgAL5KxwAYOPK8FICBVmfCJf3kyQSkPUEnIyc4hM7UeKO8CgU3IHQjSoCvtwBsjLRtEmpAcKIQsOAXzxnI0UiNvVnKyjFy2zACAIFqofIPzcAjJ/mdM3cwqb6EY6Qu/26KkAkeCxs24Rai/gipYUgUmGR8ERZnAj9EnZKwwhACQoVKyJfAkE6re4nmZlQ+5E0EjI9o0BGXyFXUnBA2jHwbSnG2Ey2QWpmsXAktz+4GTP5EaUaAfcY4ehJsbowOQWwgLViEACGhzgifa+UAEaBAEK8IGJecWxtSRwJDMnCASaiC5nGM9AUGIF0t7PECrEZ2F71AgHUXiQBj76LguRAEACH5BAgFAAAALAAAAABkAGQAAAb+QJFwSCwaj8ikUgiQAADLqHRKrVIhIBCHgYBav+Dp0wtGfEKZSiNCgZDDcHCTMXFI4IjGI/TodxYRAm5xhFIABAYXewUQcBIbfCGRkg8fGBZ3hZpEABogH318DY1hEgl7qHyoDwMTFm+bcBAGAxmRewMIjhOpvZFpE7qxchIUFXu3fB0acAAOyb7IDx0cXcNUAAwJttF8BM3P3b17GQUCsNdHABy10LcPFs0GoeLvIQt26UgACBMdku5SeWimIEO9aA/KEUB3LVsDgAdDUIgj4F/EVJEGnNPHRMCAgAE5xEEAosACkPUWcMjUUEEFlO4wEAIAgUEiejBRdcg3TIL+gQ4XVXVI8E3TEwsTjgVNGICUUQMHli5wha4JAmtf+AUYgPNgAJZx1gHNGeJAAg9ghUjw4CDBBQMM1cECYCGAxZwPMKTN6uHlxQWCyDwhAKJCB24Y4hqR4MrJkCYeQF3sALcZg48RD0xwKqIJBUXvDiigYoHPBw4a3kgAATTiAgWKl+Qhu8AAZwkKLnBL1mHiFAbIMjQwIIyJAlA5BxS1AmAC2QoeBBO4EDVgh4FTBGA80CD6YwsP60m6wNkQ1IMPGrxiIsHBgj7iKsSbwiFahwCphyDY0PUd071JZIMZSgUsxAQBBezWTQgDMDNFOMmIwgAZCETQHzJ8LEBBbET+QGAhTBcY2BkDZ7jWFBUBoDQAbPrxFxAyFxS3BAUKurPAep0JoBRIGWygAAGOTZEASMpUJoQG4aFkgBQQ+AXSAt51pkB10WSwAAgyVgGBA7WI8wAIZBAg2YsZLLcPBjX2coCRAFQE0n0ihtGJA076koFeHY0lTgQADqHBGShNkImAlEDTgAB97hOgBwVcGAI1Xjjj6B5QBhiALU+Scoge7miG1WNBHkFAAhwQMAgn7en5TgUbiSBBASg9EEF5Q4iJ0gECtFhoKiql1YkCG2RJBAGoXIABkEUA4MF7EVbioAgWqOoLlFXNU08Cg2LQjUqwSIBBAwaZWYQG9GQwAJb+b7Q5YC8ReLFamrcEgI4EisQXJQEn+UINWNnoxkcGDCSBwAGTJNTAhpzo2GmuQlhQojuPCuvBpLJmAkGC0BzAgWoOGHSLb0dA8MFulGQQgYxtSmuagwBA9eIDInESQErzAaDAP9BM8EY/GTOsjgUOFBCVNCEkkB8TBnicSgYOeAHBuhFe8EaTKCXgBQIXuNOAjBUqmMEFKy1BEwUbGLTKBmZCMOQ4yjGBQcZqXLAZER4QLE7AndE4bauuTqBgBxh8KjZu0gb73cPBJSbElhxQ4AEDBGiAAAQQhApABPDu0cCgWSeTgc6PQdjHAHh/oYG/8Fn9WNLT0hoFBFmLo7j+CMROayYDdvNxAY5gaLBNyRvrx2kvIJMGdSrXDQECNA/IOwTsqQzAuxxYhxLCB8vZHE0BHCZbkDgFCHOIAgaAEEEBHyxQHAAY5N7B9HjUKegQEjDLq7hiLy+O83K9geQvMomFAKi0gNKJQHR8WFP36Ie5AwygAQVIwARA4IBjLVBKu4nRMDyUCjANAXfQ4BM2NGABAlylck54QhXoFQmNXVAMfUlIJZyCNWhokCNKIEAGhVWINinAA5ETDAYGUIEFdOAAaZgPDpFQn3/xb4lFkAAJPUABDjjAAfiDohA+VBbsaFEJY/hisgRAAQZYQHBiTKMa18jGNrrxjXCMoxz+50jHOtrxjlbRAAHgd0co5qE1FUgUDiHgAQFATgOVe6EAhxaCdrERAQX4QwUG8IEP8EQtEECAHhU5BQlYCBXFS+MjfNEg+iUoIb3hZBQsMKAO8BCMY5CABDJ5lRcCYGbj8Bn7dpMA1zVjZpGY3xQIST4MOCAAEdhAAz7Qyyq4iV1EYCUqRDMMBuTrUXwTmwIW4DH/YKsKCHhPLzLglFtaT32bgMDwHtBMJgQgABhQgABKeCpzqiJCHsQGL6AxOxGY4RYXONoudlMBA2pnVyHY3JGSlDEWVYEB7vjAswBAo1+cLA5YIJgqLnkxd8SsTbkjpRKpIAGueK5p9DuFKsr+8crX+WMS5HkMBe4yTfHpLyAxBdUsM6lHCzBAAOLjQJpIRyH+rIJVoYqCBBgALkpUwkync0c+IaCyVeSzMwZAZgKWyc1Q8M8MvgjBBjiDIEzt4QARQEsU6GKXXnwgSq4CZi8KOAQFoKQDBgSASomGigWwzGXj7OchGqUKSkWgGor5ky/e+oYBRkOYEkhSWD+QloN2g2lXk2wkqPWYCvH1AQeowLOMQKx3hMh/dcpIzabkJQcY4WleIt0QPEDTSLBKNRxADoZCMNIiIIAbD8AHZ+gCqHEaKQ/1OMBomeAAR/HhK0zYJymzeYgI5As+BiwCBGxhlgkVATzdKMCgABv+EBEawQKpXcWjvDibaBSwWx4oDEB8ZgQATEUQpEXcLW5Uq+PdAlcMYQy8IqHQzsTQPrZJFj8EMIEBhJITbuiWAK6pJode7iBjTQJ6Y8UTm9UWFRlIAB87o0JsICAAjBxnhxUwqUchbB9ytc6LveWlD9hGlVFUgB74GokTdYaVsdqAIEWggZAyD3v0izFGEnIBh/IlklVjyX4OAjApJA1EE23fQRpgSwhYwADFDUgGfCwCCEDiIKCLQvVgkoD1cSC9t/jABVu2AZzVowMgYAkElOyOthliwjm5aEdMGg2pdTLM8QmeWi4FE0iRFARKY15C2vwYDWDusVcgtJh3R4b+PUcaId9cIawWRIkGxKkYY0IGCKiAAP+SYwAGYAk/1gaTrYFBmi+KxAWySQAurWJJU0AvQhYQAN7R5cyN9iJzZrqgVQCOX70+iUSoYE1fHCAADOCXAtpB5X5m5TxEQkUBMAHhz/RWCc/MQAcagFhOEICLVM4zIbyVOXvgI05MeCEFHpgAB5CbEwigBXxyMrdCMCYivxgOGlnNhaSqRccaJUs7NbHnFo/TEl3A8REkwIEGUOkilO6JA4yMnmtTQAMO/4J2loySz/ly3j8hC9Ey8IEIGGC5VoCoPbxE5nRw3H4yV4UL4VAaltsHT0s8BOqCLgmUhkGHF+mOxr8AgQmHUBgvX5q6Bqr6X6OtsRhNRbgkhFkGV/NhAGFjIz+aa3H4zEoWw3vH55AFx1EBPaySYIQjRn2LDmwArnGUgAAiwHX42LoUEej7BtrQR0K2FWK52MVZ2XCqPj4h4B7/dAeyKAYMWALlU1/jIRxwgQFYJAPnxoYs+ygGCRBAAebbHetFL8XQQzEIACH5BAgFAAAALAAAAABkAGQAAAb+QJFwSCwaj8gkEmBhEBASAEBJrVqv2KxIUsgsLgmQgQGZas/o9BUwebgfoUOlEaGU1fi0WW8I+eGAD3IJHlF5h1QABAoIagAKbyGAkn8PHxgWEoibQwAaIB8HHngMHX+Uk6gDARZ7nGoQBgMZfgquWgQDk7uofxUBGq9oABIUFXCUDrdZEAW9vNAHBnfCVgAMCQencAnLWBIRz+K8GQUC3tVEABwVtM8f6NYB0OOocAsOmulGAAgTpuMyQHBkIAO9g5XKEYi36VqDbeKCqWFwYcEBWggPDji3TwQAAQPqTRrlCAIDDgEKLBBZbwEHfcIeVWAJyAAiKSYNFHBDE1X+BwwwN0kwYCojpW5VpKxRNGEBxp4ZAgxsaECb0XvKjhBDwECAgqlYPIHQdVVSgKCOFBSleYDQMpMOEjSocGCBRC3XAqw9CCcD0DwfZ15dIICaR0UgKnQwKOlBB7BJ9xDz8IEnPUrSGCZiENLogQmQAUBQcCFQL3hrFHBA4EoCiA6Wxzm2lQbBQ6MLphGRQPoptAhhS18w0KiTgg8sJQ0gMGxCzwoeJBO4sHeczSsIkAtqwECyhdssL0C2xqE6tAathBBzsCD2ZZJWLIT0EycA6yEaNvgWlwEEWiXXzCTSAxsspB4BXfQkyF1VCADbG3BwtwcCCbj3TAcUaDaEBBX+ZtSAgR4xAB5CC0QAwktrGMDTNgMoABOFNDVQXBUUWFVPBel5BNJ+u2SwgQKZGLLGPKYB0oEBe2hwAUsZOGAFBIK1FJ16atFHzgLAOALCU888AMIeFlSWEYNaqShSZuoJYCMvC0wAoh4IOBAlOX/puBJfEfxHhAadyZanegwcM04DAuiplZBaebATPR1wYAYAGPCIygLw8bNlPSFUUJwiYj7z2X3YFTBAAhwQYJh6EjgAW5cVcLRFaSL9eQQB2o1zAAVDNMOoo0V4AqoRXKAyXI6dCNBeL5J8cFcpJE7Za0EiJTAVAGayySsRsXyQzxIJ+GYJCBq4Ehg9SHl0KT3+TS4jwZIHUWoGAXf20qi4FNEibRIOWJgBoUF9dOwuBwgwRC67VFAACAoIEK4RHlgYyAT6rGvhAdcKkaqNH8zI8AeLkROBxgB4sOouylKZgAMUmCqFUloFIFIHzHlUJS8TuOKPvDFrZRIIBZi3wcLqFUROVodlIcG/4pSLAKy7yEgEAhH4tq/GS0BAgX67JMCgBBtAs1waHhzAFwPqUcBjB65uMYFvP50KIG8PuuHjjEzo8gwGGvITdT0N6AOAAwPQhVEGEHfigD0DkI2Gku5IUq5HQttTwXhPsjsO3hsiYAEFDkwQQQI5i8CA2I1dQOwZGnQbCG1C2MZLCLhqIV/+PR1Uqs5We0DAdAgDnH5GP7t/kPMj0BSQt3GMFU81gBjY2IHvtf37gKxbxAvIAqEnBYJINWOhQWWS+PWKmtcrLgQI5CD5TTj0UHz8I09dsDweEETQwais7DH6M9RX4To9CzBfFYIFB/e9QjRPgACiRLC0XTxAft4T1HnmhwQCxI+C+wDAPHpRAQFSYX9GGgAYJsA6KyiAElE5HlWSJ4lbHc8DX4gABhTggSdEQYUiiAAgRNGRJMAwFQ9QnxUkAIWVOaIzDxgA5XqoHgR4wAATaEAHfoJDREigAR+4wAaEyMS3aeBXXVSPBRDgtjCa8YxoTKMa18jGJRCxilZcYBv+CYCBCBSgAGTq4iO+QEIPaECOYQTABQziGAxaQwqATEKNCpY2MyJAghUwVBJwogEGUMAAAdiAwK7AAKS5gWhnVBMlIvA+AnAABBHYAMcsowDvgW8S/esiOCwTO2ts4CJWCoTtlMA1XgyATIgkIxypIB8jGbII2UHGiuAAs7AQCRUZqCUDDYDKBhSOExLYIBy6x0lPosJpWHAQzYgQpj9koJWcYJYk0PY+DqypFxFYYhIQ4M0DgEWDllnAMbEAgQYE4l5XyCZCvpQFNkADc0LQwIguADRYrI0SHdwNy46QnXocoIScFEfJ0jSJDHyMfueSBEEtBoIAYIACY0TUR1j+uIvenUECdusoKGfZmHLsEwkQCAAhJXEBmDwCNhnoQAU+0IAAPEqb4hDP7zjAo8RNSD+VaFUikSCB7yxTeHsaUWNGCoGRdQmUWVCoODZwTwTF5gARKMQQVdVRCW3oobsI4BBOeJDn8YGlIfCLdDZgjxCUaDWSFIFCSeYsIVDAPFGJmFZTMQBJrowYlTRAhlq3WMcUth/dsocgvrDJI/SydG8SAQHmBFELUOmdu8DAESBAAA8ooI4fSF7ihiAyZD2gVa3hAFm28YDHFUGgt/WPOspJjmvZ5kx5DAxqw3dNNoiDd41URASQ5oeeLgEDHUgAA9ASJnoUwG9ViZWeOIP+qdoN4bjQCOAtJMCAAAjGDeA8ggY8QLlcSGoBphVCLs7UyLc6jBJ9U48HJNiL3JxKCggQwAQUg55JrtcYECkg6wCgQ5GQNQkWIK0DtyUz84QvAdBrIgECqw4EBGC5guAw8RCCoXhIoA0kmuwWMPBfS0xjmLtRgD8vIxUxxnQcGyAxAlDcGKyS9EygcYQACkA6coEFAlAdmxWghZD4SgC7zw1BPAsqGgsYQEwHiQpYuBZhe3CTCg0c0AZmJAEOeHObpVSAAzawFos6YMwwFsnXrGEsmnw0TbUCRLoCGoqyLMBFuQopo7ioPbz2SGudSJ2N4oBOK2igLOUgVk4dbQ/+gH5jd5e5wJsAQIHYuiGinDRKBgZgAJ9S6CoZQ8PsjEIoVxBgLEm8qRA4kJE2EYsJXaOJeYdx2Kv8pF8EiAuJiYBUgLGiXwqYBU30ShAPO/ABBcjE7YLTCy80YDXiIoAOr5LYPKSqxpTwAz5C+432eDsBGNhurxAgi7I8IMmHePFV+vLtMibCADP0QBGPICIi8yIBun6STpPDkwN8wADhep8RldAHeyNcGK5pcnLWOQEK/BHHDFMQ4eRpbqIoSJkZ+MAE4j1wNVx62j3OIDvs3e0FfKAAuzSapCZxbCYyoTQ0L5JR1QBTkTSgsEzMqVfL0phYFjQBtro4GovRgJ12s6QACefH4RzYIpL3sB/5Qjc0jDwMutbUTctmIgES8Gba5RwLHsBIBwrQnTb+VgD2szdGcWGRBCDa7qv1gF5oEkSQx8IOIPd5PzgQCk6H4My/myjgk5LsCwzAPN+dPBqJQQAKVPNBC/C65jsC2dZS4O+jF0EQAAAh+QQIBQAAACwAAAAAZABkAAAG/kCRcEgsGo/IZFHAUVA8DAICAgAor9isdqsNPL6Pw+ISARksCCt3zW5vKaH4I/SlZwaNiIDq7vvdFh1zg3SFX2J6fH+LjEcIBYaRhHQPDRgWEo2aIlV+EhOEoYaEGRUTFmqbfRAYBqlsAAYZkrShpQEaqrASFBUPHxB+DAuixZFyBxxpulkADAkZcxkEfggNtdiD0gUCr8xGABwDoRR+ABt1xuqUDwsOmd9FAAgTHXKFAX+y6dm0hQcFCHjT5ezavUEDBnJBAGLDhQ8LDvBbJ2qAAHjMAHgYgK1DLnMAJCCwoCDABkH9inXggHETAAUVsmUQ4HIeAwMFJqbsgKEl/iMJBiRmezBBIZIqna6EJDBhwSyKhTIECNYollCKF6geFWlBgAIDDlxpmQfCF9RBAXy6eXm134ePRiR4cJCgQYWrBdY4C9BW3Zd3fwAI8JWyXTciVQg4qNAhGqVCGNrI/aBTpdi1DDhCPTABAWIEFC5oo5XBw5akQiSAQEmxgwKjWKydXWCgJQQFBZ5mGwA3tgLPQyQo+JAyxAALbQBMKF7BA0YABAocOIgtgValHDo08JAKgIUGZ7PqzZ7yAiohITEQqyzpgQPYRSQEiNMBVyoE6ChKVZvEWcx1GQSkBnS5xZFSBh18cFgWEoA3yAXcDYFABOzNEcICFMBHhAQJ/lRIRwMCoceAQR4ukIAZAmCyBQHTWfjAAApghB9UF/SmBAUtYlMBAwMOphtpBShAgASobSGAShikosEFKUWGBQT/YbNAhJwoMF0tGSwQAHB+eEFaWkN859cD0yiFAUUHXCaYPcbUd15gCWSTJHoCsHYMHRHwN4QGxPmVp4hRitLARVkgoIEiSDBQoEqvpXZmMV8M4MB18oCgmyghVAAcdJQVw9kyWTjQwQIVJMABJt4AAIGokDanhgSi2YGIkGnARgBx2BxQjhAQbFDLSqkqocBoD1xwRqoCEHPnW2F+kAcHGugJjizZJAAPAGfSsgAH3ggnLQNCGXKHfYgJoNko/iFEMOAUGsbFpDoVLEhAoIWs1JIzFyxADRIaXDOJNIPem6ynNAlDkbqpxRpKMq9I4MB0GRjQnwEfMOZYKJ0h5oGdgzC7FigdIVelPZIURQQ9hiB8lKpzFcDmYwmEiN4+kWTwnhtQVjsgAyC4bEgDXIow4aUXBN0fBBRIJ24BvUmAjiQD7MuGB32NwgBiIWngAQZ2LSjCJ5d2UNtpwgliYQYbcOndOJLMqZcX6nwgLSeoAeDAQQNc3caSlybQXVCSVEBpFhC8W4vbWoBriHntJqHBBhc/wK2E4B2zKxcWnCtKB6ZtUbghxzXen2wdS/1SMQWIjp4CF0tS9Gk0P9AB/o+MIPAfgjenpiwh+nIBAAjZmKwFn4Nk4GQjAlyQAAYMICqCA6KkqboEEeQ6eTOsFwK0SxAQeYTiofypBQIKB643g5DMcUCj8RRBvutGK0HA7lDHnwQB6bzevjxe8i5yFhbIkaDmVoRh2SEAqmNEOFoXhgxpwQN+2YDqKBSHA3Ruf0XwwAIkcRksCAAbGQCB6ACgmQdoCoNGuJUkcocFBUjiABXIw/+wIAHWfCCBjSAfP0IApixwgBIVKEAATuW82LCpAyJEoTzmkoBOPUB8WODABUAgAAJQAYdCeBwIKKAiJYJDAhq4yQZAQMAhNE4wDqDAoYpENyWqqisOON9R/lYGAQJQwAEcGAv0MrWBIcrMi0JgwAUEcYCCnQYCJAlAATZIlLFkaxAAsZESlzOHDszwCrdJwAUqcCkejsUAO2wOINFjuA7YDwkCkIgxjudBBmbgel60XSEGUMYhYKCThMhj4uj3Fyz+IZWFUBkWlFMhQ2KhX5KAIgqpl47LDdNwoijNFpxWEUm2LzOQPKUjKPOrSyoBAHATlzOx5sskyIcQE6hlIHn5M20eoU6TCMEE5AEBBtDFmItgAGs64DXsCDAS1loIL8mklZdYrB3u5AIEKveFgGoBbNhI4mkoWQg6II54cqhROYcAgf5l6oJZQGYt1odDBhTDY5ygQDoy/hCBhF5BNQKUaDPqhI3jsEECmouDzbpTveIVwKVIgMAEBHiBeykFeNgQz3guNYe83Sc/QNyD6qDTgEt9QGoi0EAEJkABK3pvQxwTl0y5gFEXPWADBSUAVOlwgAg4Zyy+isoHqMSJcB6gAQE41hB+aIwQHMCbn/wnmRBHoMAlQAEa0NDvLjbXV6Qyem5rUDbk1gdrHEN2dJ0HNF4YQ65K67HF+qMI5kULm1apaoRgZRuoVowF9DMW4xhNISqAVfeRalKIsUCfanYZy6rjANZUylfBSZ2mvpYAEVDWY76AT8Q4gK5CsAA0DVGAawGur8r8Jhg5EIEGnA+ZxXBVfBgQ/gB6PUC18kjhbkNRgf8RIKcL66c8uKKABNzFMQ24FgMIIwraUKoKCBDABAbQgQMcYAPT7MWvrgcACqrDOv1RQAQGwEA7AIYT5ClGBhLwJnCMxAPyPQIANMAXLF34dNnogAOP4lFMqXhAj6pFfn+iANGoYyro0W1xDbEBAmpAsKMAUXBaXNEbBsYDPquOVmY0FJAmgVrr2J6jXqYNByTnjRQzkDEOgOPU+IoiCCxU+SCVAC5JgAPsbO7RwsgBB5xEDr/FbWpAQZGojWVg2AgBS9UmgPUCdwtahUjVarGAGHHUUq3p4DAtlZIym1EDCWiRUodppeJIg3EcDQAuJQFh/oXmZCghuMAfAUCBD4RwhERWxwA4UFD8FEfKa5jXOrTXT8VAVwnUTMkCTtGd6JyFc2tRQFhD8YUF9ARrgObvSAPAgHspgMIHYiEsOIDail66i2s4koYX0AADgAo9E3KRTHrYB4dturUOEG0WDCCuMWzgEvyRQE48NIgJDE4yIIOKnrv97Shu1QEYUIAHEPBVEW82JWjVhFDPjS4yDcAAiW1XSIjExifT+wGO3gRMZ62NSnI1WhtFAotkYu+MGIDKeSbFByJggIEX3BOMJA0I7t2IM5u3MHpewAfIgAEKALU/X5YET9TZB+/Y2NJw7ngGhOcGUEpiOyFXaABifpbihhaL5lqwAD8OkHEv8qKqljbGBwC7BVlGSgFYb988oEfvHctunGvgEJkioO5RjjYB7NR3CMY6ng3Qzu5HkEsEhj2UTrtBAmkHvAgg4IF6VL2pwVW8JgDMgQYcoMLFcLLk4wEdDFyAwNmQ2ObdKAELUAAEdVkPj0dv95AggAAgBksAIhBm1gcBACH5BAgFAAAALAAAAABkAGQAAAb+QJFwSCwaj8ikiKDgGDgcBcXDICAkAIByy+16v0pO6DEePzKZxSUB4jAgWrB8Tt8SDmVyKE8+VC4RFHB1hIVgEAVkinl7iw9+CR6DhpSVQwAGD46Mm2YfGBYSlqOFDAucqIp6ZxUTFnGksV4IBam2emMZFQEasnIAWIUAIBm3qbhjBxwIsL5JAAwRHoamxpuqixkFHs3ORQAcAw8RhhAf19aNjBUOEN7fCBMdeweihAAOGWea6P2MIdo0dJMFrUEjTdMKQXCCIcCGBh8qHPDXj8wACgNHARAwwEyjCBnpZAGAgAGHAAVOUeT0oAMHe7EAKKhQJs8ABL6yQGBg4IL+pnSKOmCAaUmCgYmoOlAQOXLkMwAWJlT4ufLMBKKGMOHpNyHkNwkaLAhw4qAsBgZehZAEMaBY1QcBsNaRibTfhV5KJDBwkOCCRLe5OKS9xEBeOoAg5P4SQJNiBwEDsxAAUaFDsT3qLBL4AszDuaodDAxWAq3jygMKmgGAQMGnI36MEijeAsFBB9Cp6yAwuLLCUCIQFBS43MlRBgyjj8g8ZyzEAAsiJ6RbwO0SgQLzgIZYsNkLSVhQG1S94A4MuHkUL7xSKwHDglVAL8w+AmGXwCEINlBNlSEuZwaN2aINAXEAcB1xxqQxwAcfLPWFAGQ0gBZ+EezXyQIYeSFBAhb+ctIAgWoxYJA1C7BhABUaaDCfEQBEYMYACsCU30p3eUGBPrcssJ4IG1UA2DUZbKAAAXAkR99UjQgFiwbi8bfBS13UZ0wF1fGoAB78LcCLJRRs5VEAsBDQpCaQCMIZBj9ycoBoagmQ3TUdTABiJQBMkKYmyLVp2QcJKHAfGBp8kBknEdgDDU2pNCDAiiJAkCICkySBwAWDkuFSHBJIoaKRRAAQQIeLVIATjwR8puYEzHDBWAesVpAAB0QO1N5tqFBZIKdGlOrPAQ42mkhSUHaBQIBUXYABAVht9B4qH+AlDAeglpFAeZiAuoBg3zyTAH8D8KIaRyyFAJIhEoxZa5X+CCzriEtEQTMhEgKggtkDF2R4CWP/PCIANdFqcpV1DnywgFtrNiOBAwcUMB8EE2wg8MD8KJJBBKPqeU2z9wRwSwfQfcUASlo2E08uziqnEwMgYKfOGAn8yaMBOGbjAK7AIYlOAl6t9gYRCEQQMwecZbpBzGNs0J0QEGz72gdHy+GBl9cwIIcEduZRAK6rKaBu0RVDJQ4nef5SNToNMPqNA3wMIDUdGrjGD86XwLzJAmYbAQGl/YTtBQNQX/Au2wkQlwG2SPPGSK9fWGBaUgl5cfciz9EMz68PNAieAtdcbd6VthRQMReYxNzB34VMagCy3UhA7BjcmQfCLWB+oav+GRiQkhbaxhHehQQuolOwdxwAdsHn74jw9GuFfjFpPwus3QUAG5ShjOSE3P3P8F9osLUjHxCvBAGLYF98p5+u4zwXfNtS9hfQmgEC9fcEzwhqRnpwpxkbcFrhGB00Pv4QAtjeA9jUBQrArkBbAMDiRPW/IhDga4uY2TPAQgEGYI4/tSOVB1J1BAmgJwQfgF/pzEUG/3iMAwmYRxtuATQeYaADFwiAJLqBgA/mr3irEYCzHseIfw1BAhYwQAHa0ogAZII/LcSHGTowABDMaQl1mcA7oBGBAVSgVxKohSOSJwIJcOACEFtZBI7oCD9sAC9KXEQIGuAs7S3gAyAomSz+FCC9XgHAAxtYwETIIBshWEAf13jAq/KQgQ9EgAMuS2MeMIY0A7xBhF4wgPT810UNKKCKGfDhH20xATo+oBUKeEU3MMGJ51wCkmDQmKU69hUmQEYI9uuHDCcgAGaEBByboFIDO4W3EHTAe52CBYT6IcECEg0gumtguhRRAU6hyRYt7AKA+PCAYu5SAHUZ1/P2hw7EbSFQhKrbKHhHFQVwxifHON8WJKAfRgxAjsVjAAR/+QVz2KJ13vmUcbz5DgmUjwxd+YIp+tEAYCbBTWoMgRQbWA1LvfJ5HIBabMQphGXyIQPl6ZReHADPOpRLFdPijCpTkRjzTOAaeltCAKb+Qi+XefSkzFTnN5kDpNyAgQHMctZGqDKxjtYTBFBzAEU3IlHIsfILEoBgGTJgTXIu1XMv7VtGn/dPVJBnDuC4k9pgMSNVVEAAkfLCMxXBNDlAQCWoyMD72Ga4RWwgowYaTk0OMAEPUNR4jShklXh0hS2IoR8HOKp55MaI44DnOqoYg6v8lBwI0OoDpBNAAw7JwSKU6xYfuGsRdsOS/oEHAtyclx8aMAEDKGCq36jQBZ64hPewYgOCuFxdUJFBQnjgg6v4qsE4QFNcrKK2LNrLVKEiqE2MDj9tVZNPOSOdTgxgr6SKAFotJJ+nOJCEZbjqy4paEy4WgknhspVlP7b+vT3oSHZKXcR5haCBxV2DV6jkEYD2U4YFGCBZOZRKBw7A39CMRgIUWF1QCNciioQUq0B8KF3QobanIEAsFKAAa1mkgQDglpBCVcsF+6EUmkHFABsYAIYwJclriK8SAEYnOgJALQsU9xgbGCoCvgixM5RVCP68RuwocUeV2aKPFW1nRWSqnCBitxFsvAQG6tISSppnNUEUVKWkZ8JGCfkYO6ZN78LFx65xAFGVQy1pJAABR1nQARvAre/a8cOTGmMATSNNAOUlMYrd62tZ5sKkLnABgc3WHwuI0Q8DcL9F+Jcz+bBFIxLQNQ1EILBgwIB2JFaAHTXKThQRpGYRcYv+n3wIFhKwK1Jf/JYBGACuCOCQNQpKhwcqWhEXeOgcPDDdTi/AFYeNnjX6NxcF0Moxv/lFok8DAgZgZb4rMew9DHDhQGZAPXfldD/S0IBlZGSgxjhASQshAWL4Qx0LcMCEk9BQQqohAWdZEQSiR5EJiJkOVMv0UhtwatBZIAIJwHcAQGBaD2wKdMNOBaNHwbBCZ6YPn7DlBCXAcCxkQQ7lfs0GDEqIbv+Zzo04QAAo8G9ffJQTmXw3JYzS7OYcwJAnukJ8OyVp44BA5HT68qSz8cbJYmAKFiCSIcCXhwME2xtQcc1hsMEPNBhdm3MxzQMktPIoWbgq/8DMvJxDcS+GOOAREViuMzIlHqgTfT8H2FchPEBvmE9RA/nwutSpGdCKV3aXRbh3eSuixghpHe7k8kAEft2cxLbEXnj3BQQ8IA+112StgXdGFhBggAYcgGgr+YDZE28JAzngAgO4eCCJTHmCSIAAFOBLA1wrLxB0Hu7AQAABPDAWBwRgAglIwAQCYNPEBwEAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPSKOEYPEIFBwDh6OgeBgaBAQASHq/4LAYzFgcHqEQeh3KZBaXCIjDQHTH+Lz+C+mo/2xrD2sZFQ0RFFt7i4xjAAGAkWhpgQ8ZBxUJAoqNnZ5DHn6BgKOTlA8fGBYSn62LEBulf5Jqg4CFExZ3rrxgAAYHtLKTbKcVAQi9yl4IDcLPlaQdHHbL1kMAIBnQ3JFpGQUCu9dF44sEwcPc0WgLDqzk2AgBGp0AsRmW+rbqo6TgBMwpA8CgwQMMnghgkBIFQ4ANDT5UOLPOG5oB4q4BEDAgzQB4vLgAQGCBQ4ACCyqOWsABZEgFFWodoBCPCwIGBi7wU9nhXf4vCQY6mHpQwKUekUgFCuFiYUKFnf0yBIDg6hdFfhU85AEgQYMFAVEciF1oYFMSABpADNjWj1IAo4sAcBA6aw0IuEggMHCQ4MJEtkM/WPhCcIIoaGswKMUDwENMWQ2SnQVAAESFDtvSaCb1YAPechI8GHxW60EHA4vDEOyojgOSkRR0VhoEtY3WMRJACC01tIOC1F+arWsgUIKCAmy9aR71MY/xD8PYDCCwB8CEis2xEbhwVRJtWgaqW3DmL9CFz2eB9XvQgAE2CBgW1C4mbEE9R7sQxGL3JkBAxgywBk0B/4lA2X7rWNLBAANE9IFPYgCgAAP5JZDcIB1cwAEneP5AEEFbFxRI0AeUQLNAAiAY4IEFBGigAQEcgkEAg78Nod8aFUyQCCMUXEjLArosxdF8f2SwgQIwctHIL/mctosGDVSAAQESAMfHY88s4MEdEvohzBsgSNYKArINAsIuVXoCAAY+BnIAakKmQ0sH/lmphwDBFIkBentoQCI0E8Cz2jMNCIAeABAg4KIWdhoYgTfTNKpaAJnJsgBVBhLQQJshHIAMcAgM0EEHB4yaCQdJIoFAPpxVkFFCJA5zgADYBFXKNHwKIcF1pNRywZRwaSCfRR/ct6QBRKKRAEiIChAAlg+w1OhG3dWVwQBhjrORgIFEIGkSEsgmTFbmMIUBdP64jkEmaZY0QIG2AgzrJq2NeJDsAxEcStCWRBhH3WsCTBDBBhJhNosaGUQgpoGh+FMsI4+oA6QeCEyQgWJeIAWBBw4UcAYxISSgwS5MVpKBA98WAQG0kiSQsggIRLBNAQv7AgEFG+QjyAb/CgFBAhZNt4gHcpbiHh67spXBu1sZt1stG4hJGbe1YLyVzITmWo4DpKC8iAYXJHcyESVHUgGmHV4AjdViMFD0BUF+bWEGDYBwtI2jFUNTHhZQzUYHt4kBgU5/DBB3XAiAoIAGcEnoXQEpS8hqKTQ7YsDkHVD4iZKvyfvHAj2rBgKg3/pJCELxDMG1m3CKIcGjshzAgf6kkv8RWepDuC1JvuoSXsoCd4MhQQF/yP5yI2SOckHNRnDFAAQaPCXMB8wnQQA/y+OOTQCjVDDYEVxZEF8GVxQdSQPwSFBNEgoUGcDxS3Lg4wE1km1cBAtkZkWyG9zhwQX0WMyH1AA47RHBAykJROsMBAEDNMBLteCAAKDxPgPJzzSaiBEABHSpmoxkFwSIFSC8JgIIBMBgtaAEBto3DNT9IoUP0BBIJHCYD8AvQhIQAAg+0LNmjOItQvBApUqEhjkIwxLhMZADQBaChwkBAVfp3zUIUAChHOBug9sdPCwwOUlMAFm0UINrlCgJJ4pAA3R5wATIwUJL7E0E4RrFsv6EwMVhRMAAR8zAGAGwxPMZC41/8NY1WNgpepWQeN3aYheLEQEOhLENe+wjG8wIRUsMYIzWwMAaZjWEOEaCdyJgQLImQMjZuBCPgRDaE+GgCq01AhJo6MD34GgAz6kRHkR7RgAo8KUP3OYXUNGSdmK0DACoLZYLGwkHPvCxMwmhR8/AgACgkglDEYEC5qPfDRmBAHlVAC8EScAAWoesYdAhHXQLwCqMwACWPYCEBsTTGlx2lhZ1wTpD1BsBUnKBxS3GdJ9EG+4A8KE1GNIXajuiByCgAF0ARwKxiMQAjIW7AP2hA9VTlQhXEjrCcC8QSzOgBD6qBkG2zZaAoB4eBP6QxkAa0AJp7MCrfMEB87EhAQIFQzdHcYCclqMVEiCPGnDqCFgKw5mOmEAp2IYNCVjAn53YFVaCBwYoPUObeWAALcyoKwpEoAIJo6geclM0CKkGT88w3FgHMIqTjcNW36hcdRTwNp9mbHTDuIBdCSM/SQxAcz5DTmlclaasiiIDPMxDN6GB1DxYtRidESgVd3KACHjAlUT4mRp8+bJSSkKWcblcW9l2oF4toJEIwKxcDsBZ5wi1FNnZg3BWwq+lICABtijNARbQgACscEKpmUeBnjiy1ACAAodZaidCQQtXaWuZuYXsZsX6U+2sZQMUWF8R1nVV6m5FqbTAiLYIgP4/dRhPDONRWgMcsM5acWoNoEQedH40y04y4FnP8EwYQlgihFXArDMyL9Ps4RioDCJwZBtJwC5DqgM4uAMdVQIFUKoGrDqqH3PcnAKSm4ZvEmYkX6EABQTQXvBp4IS8OZmgFNCPDgz4ExLQZCQaC1QF+I4WU1lK3/pRlM1tDxATq4oHqggNovososPIAFW3srGBGWtNH4tvXBBlAQNslBaeQhtE1zGBG6LFABYaRNSwwYHLvNgXEoBAmqHHAAV0zEvmdYCWwasOVW6FAAH4QBcTJrWA7RV8ArhAAQR9gQ/kD2TDqMDsOkkpbpy3QwLYwMe8kwCpFTYMwzuYStoAt/5dQMBi68lwUWEojBDBjwEXassaLilQ66hapXqYUUUK9TLrWAQxC1BnuTawjgJWB7mq7sCe8LBYVVc4AAz4DDSTzNStqAcxnC5xxti0jgx0oAHUAM7gkgzERsT4vd5wx3DzckzvvKEBCWCvK9e0yFpM4M9Io3O1P2AAYvaLAyAQWAIiEIEJpIgOjEuZrOWY0T0kTSWWSIUdlMIFCTg8zUiJS0EBUemfgKA7soiEpygQ8NS5jRDvXgZQONyWNnzgjh5I7TYJQx6pwNseZd40bdCw2w/EYYUrYkITCj6G9gkbsxCzQNg2nUc3GJ1uK4dZAmqbuk8nUOa9EkSFl9w0A3B2dVNQzzh8k251eSwxWVlPbNddQYAEoNTYo+X62DspgAi0FO2SyN7aXbExFGf9Fgedu5pG4sADtNvY9NR7LyjjgAsMAOPV9q7g1bQENyegAfJRdRIXbw2uIIAAHqAABxwQgAnsewIBcAAHrMBzawQBACH5BAgFAAAALAAAAABkAGQAAAb+QJFwSCwaj8ghACBBaBgegYJD5SgoHgYBIVkmv+CweIyUKECRS+XwCLnb8ExmcUmADAwIgMzv+5EICyFwg4OEbYYPbQcVDREUen+Sk2AAAYeFhZiahowJHl2UoqMagpinnIRvDx8YFhKjsX4SIIqpt6i2hgMBFnuywGEMA7jFm4hvFQEawc1IACAZxtOHhwcGkc7aIhrEudTGGQUCv9vBAA7f6tODCw6w5sEIHYoPGfb49eC54gTl8aIAKGiwIYADA1QMYAiwocGHNbbWYRpAzhwAUKOYeCmypAkDDgEKmNpXaAEHeM0EUgRIZAkEBgYKRNzXAQNKWRIM0PuAQNb+kp8/nwEgMGGBNIkhMgSAAAyAgQODrv3jw0SDBQEcDDjYepADBQYaQnHUAGLATHUBbk4S2CFTg559XjpIoObA0UwPOjRK+4xBgLbq2mSwGVBABU32OEwVSgBEhQ7S3LxBLDkEhsVKJHj4cJZToQ6K1w7zzApuEgAIKFyopu+Uog5Mw9Cix2/ASUoIGtxyw+ELBAUFIquanOvyGIEfbmX44EADZjIAJuBqMyA2RwIF2k6f9oEZdAu6CS3A4C8gB8C4Bv9jgmEBUlQZHDz/omHDvQrN50NncNjYgwkoDbXBe3MMMIBDBtLhnR8ITAACAvrxIUECnR3SQHkiAMCAbtT+LGCHAR5YQIAGJFoQYoRFSBAginxQANU3C/giBAACmBXOBhwQ0AWLYaBjAI9xHQajB7+wRZwmcyyjDQP3BADkcQaoI9WMAmh3SgcTYJhSAoYYoJYo3fgH4BASSIdKAwJ8qQQETmiAQDZjMMDGIKA9+QU0d52ygGkZmvFBngcEAGEZGAzQQQcHHKpXAjnCecSEkQ1SQUWjEJBcLgdQUAYHDUAFmppKGHBPKnBcYICMRtAIGCENLLiWqMYkYF2qCBiQgAIoQnApaXB8AIJzHF1y5AMJ2GmEBBcYswCRlUAAKkdR+pcUmjddJCQnBwggigcVDpJBBM+e5lsCGyQQQUP+j42KZAR8XkRPKt2tFUAxIXRgAYO43gkUABB4AEJ2mQyyAbAziopJfMYKIYF70xXbR4P2+sEvBfZFRKyrEHA5EQGSeDCnawz0UeY9CYR72m+0RSWfEgR4g5hxfQAQQZ6cNGAyRw68sQClf2hwQQYHFMCAWk6NCscDFcw6BgTJFveknIg8sPIkGjigAKi5eRaCpn1Y4DImHXjAB9NHD4CqJEE9owBrBbAIAVMCGZ1KAXxWYjCdDCQc80iSLcDxaQhggN8SIBgzAZAa7DoYS0PkfHQIU6aqgQGchQCuBBEUc0BoGVaiwF0X1B2Px9WAyxEEBjQg3Fu5dbtAyCLkROT+cxIUUMjmev/BtGehZ8bBBy+WNGIFufAkROsXCOCoEBqALno8EghLSAWw02hjNRlkETwmNgvB3yIbUKCWAquAkDsZAXJAc6Z7yIzKIAJwO90Gv7hISAcJuBoBHB1oCz0FESiHB0YChx8JwQE0IwQHBGAMJ2Xoc5xYlhJctgClBUMCFFAdbIZgqVRMbW3FIR98HDAjDKTCbGRCzwfOB4ZoPeAAfxNB6zhxOCF4QG6HCIALSWWAEmLiAzFEwMfoZw4TRgV2IiAbDeHBpFxMYIeHyEBvMuQ4TcSLee/6TzxcmAGuxc52hyiZECyAQ01EAIp4UcCMqtirBWkAPROIBwf+4JAtMq2GExGIDRlzcUZ+TBEdP1yQEOHgsG0Y4HZiE4ISCWE6EewRFxOY43QwULATxhAC6LkAC78wrzZE7HgNwMSYRHDDXASAArl4gAOdchYJKox4WHJVSppWL9MkrhrmEwIFElgIDAigW8T6hQBwmIF8ZSgAWdrkFwIBhwWUAwERCN4gDCgCDPCyDRxoIi4uwESGHW1qSVQmGATwsQAqAQEcWI0iitm+/RWDAgTwZiocyI3KMfJmF9wfHLx4zt+FwJUAuKNrPDDDo+HvbBIYECcGIEuADIN/z+sTByYAFwTYMxX2gkDm+PeJqViCZl1knMIugYganmYj/CnGWyz+sYgEpAkJFEAPHCIgUhEwIIv94xEAOLA9POqBp0JbHhEgcK04WFAbEgilIWSFPundIpcIONtpzMQJmD1jFJiLCPX4cEtcHMCYZGDALa6YKgIooKFUUcDHHpDL4wwzFygU2fUKgbDTFaoDdKPE99zATao4FRMXOGol1HfCvCnBAxUQzqTEErMJ3AOID8siqcDJ1fBwYgPWEdBMDhABjPjhjQ0wLB/WVoxPSuxuhFAPyzZAmQVEgANcoEo27ZRUYwwAn4CwrEGZNSMEJCBPijjAAhoQAAxMQSuCnZGdBPKxW1CSEh6QaTJ4FjsOmCVqeNEEZU+KoqxhCq1UoaoqmNr+EgJEQJ6oqEBEi2CBACgguRl6ijEaSQkN6NZbbU2RX4o62Qi1LC8X8KwRwoQp6qJtr208KQIEMAFDIeoAED4UZpOAwaIeIMCMzZA7czHhWOwUPaoVA2quQgEBCAAsGxlLAJp7NIoqgbS56AAFxBk7DNiiOtswg0C1tpQZtcw/G8AtdIRFzXMIAGC5EOPxBqQOJF4QAwWAL1X4ZQHKqUMpKIGAQlNpUp+QaVBkgICbnCWBtz1BAQ7YgGRR0QEQWKdM6xhADDMigAehLQAfaEABLrDnC3zAKEhZwNWUgEB1dKDIa9HMBuySvz/c9Bj7EIdURaBW/5A30R6wTyYuoCX+MUyIJKcYgJeOAIH7HsJ4lJCAACIg3TagCUhVArUmFjCBSRPhl7gIGyWGkoCUXYkwPaKQrF/YC5OV+hQh7hjxAvOAArwCDAwAZirm0ADYsohGIOVLqtOxDje4o9MtMUBiU0ntBDjg2XxAgEw0MQEpjwHO1BBMtYUqQwYoAAMOmEAEEmAuByEELBmGjgJWdelRQCAA18TLIj5gAOcs5icqijhQsBrKB2xgvZKgBYvpRacJUCAsNBaDnNqNVJ3sI2rLmQAGGMAF/aT4VUKWGAeIN+ykLOADF4iAA7AgIguYiAIGeOImQ34EAFhAncM+mj3kADSmLx28NQ3zX96T3W9+vKHLUf8DBlVX88D8c85ZlxgCuE31bttjlGGfBAESgN6uR1C0ac/4qtfsdjigPe6S6NcE6F52TZgW72hDze/s0ndVPMCcgM8IATBwAUO5HYaJ94kECEABNDTA18boQAPMDfbIZ0QCGiCAByjAAQcEYN8RmEBxDcBzLgTcHEEAACH5BAgFAAAALAAAAABkAGQAAAb+QJFwSCwaj8ghACBBaBgegYJjMHAUFA+DgIAsk+CweEw2AiAWBShyqRweoRB8/ngcFpcIyGBBAMqAgYJEAB4BbQdyinOLcXRyGQMNEQJeg5eYRwAbcI2Mn42edguUlpmngxYdnqygropyDRgWEqi2ZBITda28r48ZFRMWf7fFSAQfvr3LjsABGsbRhApvy8qujHIHHH7S0RIJ19ahn3AZBQLE3req4+KvcnULDrXrtgAOu3QZD/z8de9YKTpQgIA6e5gQXLjzocGGCRisKDCAIcCGBh/cBNSnaICAeggvAWBggEGXL0WWAEDAgIPFDuJYLeAAMqQglGSWQCB5gWP+zA4Yat47eEulUaJKABCYsCCDu34OkGICoMCD1EBMEFiQYsCB14gGKDDQIAEpAA0gKvhkleEDh6s3FXTYgCDTTgcJGmgMleFABYcgKplhEKDaqw4gNMDFKkBtBgyLNSl1MKCD07WgIHWggESCh2SuLlgtNnKAogoWyqyk0DOOO1ghCkAAIwEEzHgDFAhFhaDBpwSRhUBQUOByvNd1OqQLQ/VDiA4TDEYDMMFTBg/MCRRY5Qj5ogm7NVkIQCH8KQAcVoFqUFcTBAMLOm1kNaB9zuA3GVRolYGeGQKcdKdMBgtU8MGBHwzQVD+c2SQGOJjFsQADB43UgICtLJDAHgL+WECABhoQsBUHDoBgnoNEUJCILw+AoA4AAgxwzTkKEFDWVSqhGAYE+ykzoToaNGDcJwQGYJ83ZE2HwTUHGFCTUhOotwh00tkDwQWQGaOBcyxGEB4EAvimSAMfJXGGEx9KcKKZGBzQwVtFgTBkKBUcmRICGLgxQTdGDBdBZR242UEHFSTAAS34ISNHBaOhoigvBzQIxlllaiKAYcfp88AFfCwGziIfQHOeAXP+NhsqEkTAESzyRfLMVZd+EgF+tF3QywKNnmeBWsz0Q+ZuEiTTyAECnMLANeCRxsEAHxQwQQQFZGQZOXsWQcACooQ6lS68dJCaMUyUJYJKEHjgwHb+i9SxgahCAEAqKP3R2mePrQAXiBPyEgIBBQWsGMcHDBQBQQL0EZBTTR5gWk7AgORSgJ2AMCFXPxEolpIFXKab5aQITGAAMQAEEOEDH6xphgNyuHhKx8tp8m66FZyqCQIGwFRAPVf2sjEZDKzoLSqRQXBhNiFImpIHF/ATwgcGi2CBaYdhV0bOc8waElXlPFAAUSsFcBsckY6rgNKhtScBLZO+/EAHDIckQXywLdD0uBJwwCsdD0QFAAi9TECMBxUEMMwxQ4fwmI75pJvBx0JooCo5IXiZKqRw4sPPB0GZMfaYMtvUsydeCsEA3K6wp5AvFTD8KdgXDD6EBAUMpED+vivb+skF7W3JSwUEXOtLfY3brsib6hBgHO4IecauBCI3ssC3EoiJDQMW+LtePbuGorIQCiyyvTRn2TaBEhyQrQ0FfwDw+CtZsLgBMbEukgGcQkQQTwdSgy/AB/wMoI4HpIsH40TggFLFQwEC6EUA0scBn+BPCbwKwQI6By5bScg+ispGVLgXoRBgoHuvONy4DOCJAXxLBBJQD8loBwj7gW1uvfFEAOrhAfN9IgAc6IUB2oWBUDBtCBq4TQjeZ48e1uEAbbsSKEInAgbY8DskxMb82pU4OmircVLymz1yqI1iCac1jEgA9p44B4/B4wEKoGIoGsCuICpifFucQwb+JAU70GHPeo2IQA6luMNxoewTVxSBG+Fgr3VEMQQHyF/0ljhGX0wAhKGQAwZ4CIoBzA0CX2sAC8sQgOGdsDeg0KIIaugL8mCjDgFoV/mc17a3zSFm6wBA4TpgH919YoMioIAB4eAAD5yxkAKY0wFm165wPCdZ3kAAvQYAktGVY2MGGBkHnIiNEFygHs5MFy5FQKoLoM8eKqoayKjRiAwQkzpOaQUFfAe5D5wqSKFgoiAxwCdvqM84kkKPDXHVLuHBywOgzBD0AsSIAbBrXCHZ1RwOcKRC8G8O7BECAoTlCm8NzBdvapfIyuFFBzGPEcgkAgTSUgcxii6An2BPyHz+gSv4fe2NOmLA15QjlZEk4DEMxGMYy6K2eGQgAVUSgTKtQ8F1RG8RCSiqEpQnhI/yQmXZNAeZuFYdUOxsi8ZJHSC25Ith/iGgD5iHxYxwLE8E0h4awFYdvicGGOm0I99SXz8KQKEkSABq8tumERDAjfMY8ZoR45svLiAzCnygPIvRZwnrWgQJKGAAwKjEJpsarbmRAQFqbcU2JaDUI0zUFRugIAJU1YkDTMADJjMT9WgHyYqeECttss5VEVCARzygUAoYKyESS7uj+qJkCSmcfB6oBATctDva+AtEpmCAQ1z1PNRQxiQz4QEp7WJCZjCAjIhGtEXlqna9OMBBB0H+HRnWlAARIF015ZDUe/QUFCZdGUVDYFDaEGY/AtmFV5OAAIgdQQN4dQWxJksIwB2RmJNaiQAmUJkDONjBgapABBjbWApUoAIcMIUm1levzsblAD/1MBJWYoEoILAPOCGEBrwGiQKgVhMKuMZmCGwECeSpbUZVABgdUYHEVIgAARZFaKdDYf31SyAZaMBrEcCJa+BYR2bgLRoMwCVfZCAAMnPXNR6wQCiPWADT5AIE1AQBCDxBAQ7YAHfk44kDOICCT+uFJb2MBAQEagENKcAF9IyRBSiMFwvQjRliW9EBTqdhxrQt5KxcANft1Z9IFbFINMCBoE5KLot+ygAMIOn+nq3Hv6eom5CmKoYgzccVCxBGcMrLCOKSRgNqdsQCMmemAp56EQcIAANSOwQgKwKnpEGAA74mx9aZx3enJlADDFDPnHAgERkwUTEkQAHpIVcO87C0cAwQAb38gz8LaEACMLBrTAwsAxOQtGqW8lJNuSbJzOZaEzygAAyAIAARSEACIjABB3wQoOLKxEiMRJr3IhcgkNi0YsyyBDU5/EY0NgOvGwaCP2cDb8ekAFkiTmfa1KwV3P31ByJgAIDzliXa7viI7ZaZcqQLEnjOAway4CELlHgiE7jAB/AAapVfLGkvvzirMlWHDBj96EbvxyK67HMxQMBrt+6FlZ/c9M53VHtk3unFkKvOHGGT0TtMMjTXwWCBBKAU7Mv4wGvHblcPRKDdaHckx3VUrsLcOiZIZPt9+NoAEN+dFw2YuN7bRQAMXKAycdfGAC7w5sFj5WwUAEFe4vOOcG9IAQzQsONv0gQCRKEr+E7ABCYQAAdwIAtiTjFCggAAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPyCEAIEFoGB6BgmPgcBQUD4OAkCyT4LB4TDYCIBYFKHJZHB6hEHz+yGQWlwSIw4AAyoCBgkoSAhMXAx1xD3NyjnCLcnAHFRcRFH6DmptEEgwTFQcZcY+ljaeRc5QJHpmcr2QAHIynqLamuJIfGBYSsL9gEB+4t8XEkHUVExZ/wM5EHh3G08SlixkVARrP3AAYGdThx6RyBxwIzdywEBGM1eK3ko0ZBR7p6pwEA/Dv1bgVDiDgewVAwChbjMDZqUOrHx1c9DTcGxgIgANw5SoMaLAhgAMrVDAE2NDgQyh+DwdQmEixjIYCHQoEMMCgy5ciSwAgYMAhQP6BBQ4dOcoQgWXLMQCeoDOaZAkEBgYuuKN2YILEo89yamW6xAKoqaYeVFiJFRYTDRYEUHHAFkMVCgw0eDGjwUEFsJMKECj76qmDBBdCHRSagRJHEAJcKWEQQJEqDOgG3tS0hACIRKPIoSpVWBkzIkw8fGBUwYMvyQwcIBh0hoLUWnjf1bmw2ggEBxNqU0TQ4AAGpkkkKLiQeTO/BQK4AgcGYIKcDhbKACBw4U1Qahl+8z0iS1GcD7qbQjCwoCHKsBuWHwXAoIIkOQEENiVQoFG/OwM2fsi/ANycAdtsZ4QECbgjSQccACeBVA4toIcBHlhAgAZOWOABB38NsABZAv4WQQFG9g2wFxIAUODeLRlsoAABfignAQEEnNbhEBCciMsF8iGBQAIHPHIHCOHNyNo37ziwnAQYnNjBBASoJ6QYGgzzzgTqFdRAAwLIaAYETlComCBMOBlLALE1UkGQJEIwF2gvchBBIh0c0MGcFSTAAYtiDmHBBvb8QsBo1RxAwSsEgijUe4xcgEGMY0jQQAgfBMiJLGU6kkCOmkgAwjhC1TGANstZJElR0h3hqDGl5QmGo7lsJscFHJohAFCTCECGBBxgKgQDlTIygZacMJBIBR9sEEEEG3ywQH/yyEEUsCJMB+gckYYBgAUFZMABTgG8A50z7HmwlFZPgQBTJP4HFKCrCBJEYEoGRgZHwQBxNHBPjcYkoKpZwm3Q4wGqmVqgKQDq2NiBIwrhQY/FMFARtBVBoEADfXJnwT6caQeaBcRBEkJ2zTQ3GCoNnMaEGBZNsC5r1hpgKBwLYIqrdw8VcBoEF7yjMQFMLseAnAmWBcGjqAwqBAQBjPyIiEJcXE0HHgwBQgYfJIcEznBUEB1WAChgSwHNaEA0Lh0M2jXDuBRQG751BDQRAC7PkV5ZANg4xwIjtlvMA0YCsGkxATTDgGNybMDAPX/SYcB2IKBygAF/wK10IxGo6e4tj0d+ER0PMC2ELCA2gCa4iv1cSwSneUArLg1wyaAtCzgcbf4C1uA9hAT1laPAvoBIYIC6QyDwuiO0CYEAxrZUMGF5xIAnhAY5n5KAjAQYWLw6SW0Ah62fk1lKBbIv+I4WaJN8WnuogJCOAkMFjg/0c0ygBAdKH7D77L0K4MHkkswtggDl+1jQhNCO50QNHwhgXufSMataQE4IRLoFByjwDvdR6hQLOGC06AWzlf1CAtPqgG7+ZIp4icBr1cAACm2Rnc8ZgGBbEwEEpCGHD/AOEAV8wLeM1wBUBOA0BinGHt6xrWg5wBofSBgCCOc/fGAAEgfQINZK8atd9WomxdDW5xxQi2o9zzoPkB9F2DcJo7FreHCYXtPAaI0IGCAeWjSiNf4aICkNEE6MA+GAIwR1uwKYAnVNe9kj7HSMBxRRVKfwogjsaKkbluGFk9Cg+E4BSBFYgH9wmAAZIYIBF8KQRjR8gL0o0i047FAEvEFFFUWwP8BREHCRm0UpMqgEG1XAg2aJXghEOIQo1cKEH0qhAHqlLyEoIID2CxntTCk7dSTQEQOQkQfsJgeNRRAXfBDkHC5wPmryLR1vNOBAKEA4UkXrmJy5X3N6RQELKNAUzlvktBxROT3JYUMQM0s75mBGi6Aidp/TpS1qItDvbU0C2itFwYSwoAREBh/CcgQvj5a7RojOePPEIIsGZgtzdA8vD+BetORCEQmQKX7p4I0p1P4oAga88xSiA4D3EOK+/4XSEXjEikslarXPeeA1ztKOLALYiOlJrhgbkE8CHYdLbvxNDpfCCQJOGoIFbE2mxlBfS1/6CIBGawK20NhRCHCi0pBIAQPIQMmeNwzM3S+VzUsYA3ChyJZ0TRomtFgCFPA5AFZjAFfd5x8/w1DkDSWvLYHABIAHBibIB6vVwNEQKFA+HRoAWKAj2OFIhAAHJMaRnWjqEZ6JIq0ab3UZuEDFioCAjMYhqdwRgFQOEAHTdGiTHY3h7CQBKhIZgKjxBE2S3lMnBVxFqsf9oGsb8QFgDbMCHMhn8MY2ifvhBKypeIBhJmCAKRjgEAv4QDMJcv5MY3SyCEgjbBiiMRQQ5BNX8yQFMjoFKdGSQaXFOICkQCMd58BBbY0lQARo1atHrHITcCOqUCrJCbGF4AC6DY4HQOCeoHhuE/p4h6BACxphDRBlOjFEBeJ0gBLLqQN1osBDO7Ema+WQGFH9RVKk21idpIUCAhCABSRiFJ0EgBVVokCBy8bhJ7FrYowYQHQb67J3bIDGRm6sB84FiQ7UMzgFnYcGhaSTpdQYDQaQki0GsFojZNgWNX0SfQqwh7ggQE0SgAAEnqAAB2yAZtWoK07Kq9CECUlvzgpvAwpQgAsYWlnWaZACaIw0Q5mjyASJW1BSQYzUqjcJraXDlblsAf42TuPTA7hsGSxAw4vaFTgISKg43rGAAGxWOr+F2noosGhTTe06GgYBA6B8tQiIFXsI2GcBeoET2fanwCxcQAPOUeTJqEMCPxXKAhzQJJwQwACAqYA2JXGHBiQAA7uOcqMwUJ5TqNUA61oCAjygAAzgJgIJONYEHKDCuLRY3E3ZESY/tgsv42QJEgg4nHOC71uZ6DoHCAAF5HLDJVAI0tgjN67hQDU3iuveZtAJTwLwAYcWHDQHRwnFBe3rLEjIAhaigAHYgGiKm/bj0eIYsq1BC1rY4eY4N4+PYgVzpN101eTIbj9CcGGYH7mHIq/5xD8WY6OPdHO41szS6/Brp2xbYMDwmLkxOiBSpzPUAxH4uT/O04gBjM7oEPDABMROdtlQyev/RoABerPtthem3xDfznQcgAhPL/0AA7gACIgNd5S9iAJ/aUC5HdIBb4NAARb4UuFj0QQCRAFDAZhAvCfgkQl6YEJeyPsYggAAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPyCEAIEFoGB6BgmPgcBQUD0ODgCyT4LB4TDYCIASOI3JZHB6hEHweymQWlwiIw0AAyoCBgkoSHhMXAxlyi3SNcg9zGRUNERReg5iZhAwTFQeKj4yijqMPGQcVCQKXmq1iAAQGF3Bxo7aOuLWPHxgWEq7ARhIgH5CPuci3jI2SExZ/wcESBca0ydfKdHEPFQEI0cEIxdnk2LhzHRx+4JoADBXm5fLWcRkFAtDsgwAKHfH/8+AswJBPXyAAHBRdg5TBlENIALWZImgQEwAHDUcdWPChQYEAGKpQwRBgw4UPbqqVe7CAQ8GKgSAkUBiiQ4EJfBBI+FJkif4ECBYUBCiwQN4HfDAzAdAwqwEGD35ehlmCgIEslY0+PEvaDoEvqUKWiBWLZImFTiozJNDA1SATDRYEUHFAN6QBClt2mkEQYADDCRDasoPAwEGCC54U0qqDqkECEKteArAQoIKDX4JdLSEAokIHULWs4TpVwZnesBI0YM6sFIKAWcsgkgsF54M61tEgKCgASts2ecs6UADLlfgrAgU6/G4UcVGIAKtZA7hrvKyBBfRINVcbPbO7AQc8VBcGQrm5BRU+qP8wYEFD5gW+4RYiIYEpreOLSMCQEdeCxxx4YAEBGhQIhRoRwAMJfvMJQUFvG8hXBj/+MJLBBhwQsBNxPv4RQEEEHTCQn0EQVCBKAYEB4k4DpiwQAFv7NBgWBlg9191xCQRAwIgygqHBB8vJcQBFMd2oBAROcMGKYD8lAUAANGmzgAI8OikBAQpEMEAHHRzQZQcDJJDhkvpIMMEERopAAJDIfCChIAAgwMEGxpTCyAUYaKhiik4SsEEIC4hnhgE10pFAmmMgdMA8cmQwAAjrvGJAAMa5MwsclApz6S0VCGoRArDF02gDw00lgCcCJOEBPIss8KYQHhT6SASIloFAA3V0wNEGEUSwQQOe0RRHBwbUKsI7tcFIhAQJjcJBTwEks4AFwSAXgAdejHWGBw4U8AZLVIahAa5wHOBAQf4QOPDGKBegayIytAqBRpVmnFYWBBT8ihQYMokyAAFDSBDBuo50APAQHiyKDANhUVABB8Zq1sR4T0YJCZEQKGxLBkQCEEGUojSAGQAXxAECnz3R28p0/W1TQYoeI/NAAZhBUDIyRBJgTAYTREoEA6VmBgG5zoVAQVgCgEzHv0JYMIAyHXgwBAgWrlWEzR942hY/zM0MDQF+3SKcCPy03Eh88j7dyAD5sPzAByLCxJMIEmBX9AIHCyzzuQBQjQx0QjBgHiMbEPHjHBegDI5u1A7hAHMHGPAHy8rQKrAykU+O0SgKKKEATYkbJPABIOTDgMZzxCuCB0XlcgECoCKzAP7DZCcwCt4Bh5qBS/ogsEFDLw8BKrvy/ajMAgWyioubQozrSAR86oz4q8AAMEGdUtMXrSgV0A4Ai7dkoAXqIWOGrCilD6HAIhlkyg4/Gk+gRLN0HBBuzMpkIesDG0AjAPm7y0cE5hCeimjgXQ9gG8JaxwjJCWFzueCAALKRKYSALFBKUFsIKkC9YAAASHLogISM14hzOUhpj8DA+nAWFgOMogKNo9vgPqAyQAxQSAcTwa0cAbjVodBkHLiFKQwQlseJ4gM51ECF+FfDMtBISLQTgc1GoToLmC0UEyCULeTwLLIZkQ4NUJYSFyE/mHAgEkejT6gWcaimXTF1LjzH7v6K6IgwDoEAg5tAE8mwwhAcII10WyMkqvhGOUQgiKSoQxcv4ogPiHGJ/YPJEx9wgFSpUSKqY0AhHzCBPnYNAy0cxQBiKIHBNWCPYwjAIjoQwx2KAk2w2mQAKKCM9k2OUI3AIH3sxg3FRYNkqxzhOOiQPhFQYH8OEMD+EgANCpDPftAAgO3gEKKKIICXFVjNqkbhQBHg8hZ8IB8dLmC+dzHChELQ4kRQCYb/zYGZYVEAAHlnvd6IwmgEYGAji0e01EWPcBEThN7mYEkvOmJ2YSmZEKECPv80TgJ08peyJECuAewLHE5Dh4QQQI2QyUccxyMABCIgxHSEJVrMACQA+P4zSnaWZXtwgGXTlviINh6Ll6Nw05NyIQf3CYCmcSijEAgQgBiy43LUvKg62UcRRSEjAV4wwCY3kCIEmNMYB+ATE1zqJAsU4wFQVUJE6TCtk2YjfQzAaS69NwFbEGlrDBhA94qAnCiJrHn9FAU0ddjQez6gmwxYnrK2JgB0KkEDA6TFIv+njFGGJbF22gBmJBA2OmTAsFtLEwRQegD59C0boXOQOLfxABhZcBRhBQMT7FWR6ZAOGhCgKXOKKUV9LsORwhumHAYQNzAg4EweiBg7ASCAHHpyFNVUAklLcYHekg0DGvvjeJgCiVQoQANSiRMFJpDDGNEnr9r4QHcmyP6cCGC3CLdKYXVgMY5tNCaLUzDAIRbgjwZ08CDy3JsRIFBZP4IgTR74zH+nooBEhOI3ft3GBQY7CFfm4gAMNqhAiuWkAEAvUQSIgD7NkQHawskAo02dkSxgIuEYC0mAKEQA1Aq1CThXEBzNhnSPYKaWuuUMAuhElw7AYy91IBUU0Mlw+/FURMFOMFSxgAcoIAAB9GFuRIAyGdyG3KD1CBMSUMAEIiwGCaiSOVTt0R4B4IHkZOACWwEEBP7UiCjOZ6QOePJ6z2ABA4CwVVYmA0cV4743X4+SBdgDA0QKAQn8BAJPUEC3BleNBxAroErwKhyY1iAJqMtCd/jABS5QgP5NnyQlyhCSA3yp2gAfgHfzce09dTEHepTjQvd1UpZIXZykyYxRazMArafC1UGALRm4foSL0nxlGGNAtsGuXwAYAOlik83QUc4yUd6D6wx0oAHq6DWSGbABB+yoJ7Fgg3v2dwfHOMAXzj4IBdTWAAP4DDUI8IACDACCCUQgAfcOAAhUCBXWpnsMEjCAP7DKi6iYwSeGTjhZUtakfyMBAn8OEiVnqZoaUkUA5VmwtsERpxsmIwMfiIAB+j1nr0yqAQrLwIUdHpa6BiTTeeD3gCygZAGQ5CSecLW5Ng6MuO5PiMewg9Dt4BBW3zOADl8psrHhHKzUqdVQC9e/s9wAFHs2RzRNF2KneN6Vx/386vNgHsvVBKKvJ1seKBo73TxQ9rOD/QDmVbu8PBCApbsdc6og09ipYgCUb9LtB+AFdrkuNwI4ABGMTvYBBnCBczdb7UzAEggi0ADsmOPaj1GABfQu9wk1gQBLVkMAJoDvCQTAARzIggWETHgkBAEAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPyCEAIEFoGB6BgkPlKCgeBgEhWSa/4LB4bARACBxH5FI5PELwtzyTWVwSIAMDAiD7/4BKEh5rAxkhcoiIiW+LD28HFQ0RFHyBl5iCDBNtj4uKiaCioo+RCR5dmapiAAQGF4yKpLOxsYsfGBYSq7xFABogH561tMXEjY8DARZ9vasQBobG08fFcIgVARrOmRYVyNTh1XLDiAcCzdx/BAPi7uOyiRUe6ep+DB/v+vAPFRT19sgAwMcP1KMHGRAqLMevArqAmAgIM/bgwIIPDTYEcGCAigEHATY0+NBp3DyAEP9YyFerw4UAerh48cUEAQMOAQosoOUQZf5KPwMHMGqAwQOCmWOWQGBgoEA5fz5/ApX4oEMCBqmOLNm6FckSAhMWZBhATyovAstQMtFgQQCHjw7idqTAQENWIr8clDXLC+VSBwnYHDgUqqqkDSAEWMIblS+mryAqdDh0jVQjRRkiTdDV2PFjBBRgGTxIi1GiBhyOeu4FQcEFwrKuFRSVoYEBBKtVAcDQoXTBYwcidM4dRgMsYvsMfvBAXJWGDQxB1RnwofqHAWKjy/nAYHhzMK2cilqAx0AWDegJWPCQJoFQRty9c7OwbRUABBMzbOBgFylNCQQIEEFCISzQHS8QMBcGBBUMcOAqCCSgzSUQYLABM/YZlwEB4P5hcAh3vfgHSFcQHhfBLklokA8iH3CYkhlOaIDAYhBB8BoiCyioFQiwPfABhiFKoIECEQzQQQcHHNlBBQlwQACN9iFQQI8nIiFRHLc8mMl9HEB3DDkXGADklgiIlkiOahmg3Y+qSIDBBz3OEo+PIOCWSZnaBYCSBGaS0oCLQshHhAQN/CZKB/VR6BQxHdhJhAfj6CmEm4mOoUEDhFV1UQERJCCSZAQ+gKYf0ADKwQHGcOBLAMZ0YEGgCnRQAAGCitBKAwe85AEfXJnhAQgFLPkQGRIM6EAz+BnTQD0SLFBNAsgWgMgGlYbRCgWqJWGGAHuNgUAEybi4WzUdACqCB/6oHsNAoBQkhFAB1VqbVK0iFBuqAciGSkoGGDQDwIDFNIAiABdU9kADjn4nAgTQYbkABIFOYMwFKNpoTL9CEHBQIsIpbCsGllEQqABxJtKiEBa0w6iOIMimiKSToviTBBAPoYAtBTRzJTEHiAyAAvoyUoCdELwnXaK7XaABvX8wPIHMzZKygIvFfnksAC0fM0EzDPQmygZEXBrCBWNyE2GB6w4BQiwH4Ptx0KCcWGwxB3DQBwAOlKyAEhwQdoG5vUToLgjpMJBu3Ch64CwtFyCAQKHEGDhpAlIDKoG0pfyjjjeNVFCzCGWS0rgQCAgl59QafEPLB3Y+HksCMhMA2/7o6kiQwDACKBFALBWkzWdpGWRxeCwCC8GA6okQbjNmMKujwOETKGFA0D3fDa6cD2Dx5QbNUOCGdHYPcX1FOqqT+i3pKF6Y2yI4oB0iHAhgjKQ/x5mjEiqHUEHC3AAwUQiNGoKKDBKCYwkBaMfAgAKKkQEHBApkjBjAqybVmzd8gGl+kBgkAOW6eGxNCB4o2cvUVBoDPDAWJxOCBjogB+6lBIIVSdvC+vSGKomAAXCTwwRIWIsMqMpWDiDFBxK1QkVELyUciEPPhnA5UsAOZTlERAR4OAofBiqI8CFiBUNwRIhwwBPnYCINQxCBmlkgig+Y4pd+iDcUJgoB33sAtP5SYgAl6shGsbDhGbX2RVrwK1B1jCCgIMBCRCwrJax6QwdkCIFCieKD50JjACjwJfqpiRH3m1QFFOE5iBAsDgFUIUtAoTwRtKsYGBDA++TYDJIxIgMK8BflFClDdSBgcQUq3OLIwT4PFYMDODwGxYy3y0UYUAg8bCAGxSCAOHbMVgiUTixtNb5ZUIAAxQwF61TYgFHYUAQWAEUBPueMfw1DZFeMRSYJ9iWjOLIWFZigBBqWiAEkqpGK6EDuNjfKUC4Mc6BQmhAgMMpYuAoCEaDb3my1O9Ps01aX7Ee3ekGA43CxcFuUwxNvuEliIAwAuytNKQVQyER0EZztWMCw+v5HgG7qMx1UfAO/7naqYpQRANM7xgZqdku2fU4CAejARFk6gAR8zn9Sk2ciaaE8BmRTFpKz1QRqgTHSLU0qEEiYKv2EIhUBZ5qNrAb7GFCLDcisOXibBRubWQwJBqqapsnZpApaoLI153GiOICdsDaxz3kPexmYIAC+uB0teUwDU1pEKQlptXTc0howww9ClkMvBOgiDBKgT61+0bCoimCBx3AVXmZJiwXsdXoOqmwEmKSAq/oCNBP4ANk2S4ApBQBFhLLGAM4qgq0SQ5mkS4BdwWA7d0WiATu0AgYCcIEFFDIDf/uDBjYzhL8m0AgEbWu3mAaADYwCHizin7XSQf4VWhwgXm2sxagwUaydkIMaFUlAvFRCVzl8kwgMyJ8coLKKQURGHAeYgGLsgw9SnMMnEphAj3oSovsIYAJGOoCEJXykClDiKMvUykqG8YCdJsGpnBxqOS0bBQUIQLMZDsPOAKg5r4SUv/aJMS/y+wizgkEDkWhxmzBAAd4KhAEOmO+I8puBWiaBWykeVMMq4IA9iIgmELCAAViCMIpWFTxJHgJCyVGRC4CAAx54EgQkQDMIPEEBDthARkMArywHajUIhY1BMlABjBTgAne+wAcWEEfaUMtjgfiWCAvzXSzVAl6A/oOHDPUbhEAy0WLQgAO+wWhqLGAzbl5NZnlTaf6eLcPHkGYFBELD50rXATXZCvVjXBGYCqARIQtoQAIwgBVVA6I1ULLVfTygAAOAIACdSkAEJpAHYMrECBK4pq29IqUDfMAAS1PLEshM7S482VYIUACu/pRpTxoHGR2YAAX6MyIheSCoWIrPsodQpkzFIQMfmACtj62t+1gAJx9wA0PYtG4ILIqBF7lABByABQuoZz0UMMAaSKKvWfzJ1r+40Ti4TAc6DIYOCHmHuiGN2NmAt9IgSrSNBp2cTj/AnolWZadLDjz2eYwACXjqyhldlQIY1mMSEFBJWW6oAyRAAeQMdYLQPXMAUyLXql4CAjiQbzQmJzO4IPe6tUIABzVcwEg8P8AAvFzrqVsLQBQAQQQawEJ+dEDWIFCABZDu9a9blgEJB0mnhh0ADBig4Fy4i1SCAAAh+QQIBQAAACwAAAAAZABkAAAG/kCRcEgsGo/IIQAgQWgYHoGCYzBwFBQPg4CALJPgsHhMNgIgFgUocqkcHqEQfP54HBaXCMhgQQDKgIGCShIeE20Hcopzi3F0chkDDRECXoOXmIQME24ZjYygn592C5SWmahiAAQGF4yiobGwjg8NGBYSqbpFABoOH3WzssPCIRkVEwi7uxAYwMHE0cWxA8rLmEwUFa/S3dNyHRbX2AwJnt7o3w8dCn/jggAcA9Dq9dIHBrnvgxQd9qMPMgQcWEdaBhD69sET8EwdqQ8NNkzAYEWBAQwBNjT44CbWgwQJFcJjUOBbhwsBDDBAIOELryUIGHDI2IHOBQ0iMwFAUHKY/i0PftyRWQKBQasHHxgIzXkJQoBzch4sSMCgJZIlWLFeXaVhKdNLEjD4kxrBglcRTBBYkGLAgVuKBigw0GD16z4JHD4k8HBWAgMHCRp0bGTsQIWIICqdtYuqV120qxx0iloMkuEJVRczTiVBgCvC6Oh84OBn8zULCxwtSqcoagMD1kynAmBA1L9QBxoovaT5qwQQUNNF6xAAQiAADDCENA1hgsBGGRZU+ED9w4AFAg1GiD1m1XUKvZnyDDZ1jwALBDRoILCWg4MI24aBMA6Ip6ebspVoGLBBAYGWmi0hgQYURPDcHBkoF0hznsiBQX5EhAdGMwMINIAAEhohwQSr/mVAAISzIQDCBrsBQltwckSwXBESrCjSGU6k12ISZ2TIiwBjNbKABzSCkEBp+zChgQIJDNBBBwcc2UEFCXCAi41gIDcPLCoiQcAHIegmoQT0DYUABxsUFBUoc1xgAAFQHiHBBsJ0wJcZBmQ3AHg0GoAQGRAY8EGDoLVGx51gASdMAH1d0FoHHGhGwAIdGCAhchcE580HZmVypTALcCcEA2Rm4MApQ0hQABwHtBOlNvRk0MECH2wQQQQFcNSBhQzMFqcscnDAywSxHDABTkookOMCldLoAZYBXTCBB178gRUEHjiwAYa6QDClLA0sBUEFtmVwQWwQlLTIBR9GycAF/g2QFl6N2BBAHwAYtFmuEB68AcsD5QJAwYGORNDlVRCAqhAAx2LgDgL8vpKgOwAEQA8oH+gDgKGgZDCBiwMHaIA/FbwbwTAPFKAPBBTDYrAQBDwcwgLz2pVGXw7wSYEQAAiAIiMflGvBALO4OQQIooCQZioabNSyCBBEkEhBBbhDwLWfHDAzAAokPK411n6SQWwuBYmAKw9USfPHr7AshAQf3+vAEkAPM4E7DOQ4xwZEfKnpMhvCsXKtQzigtaNoYWD1HCqiPcsBiaIV8ycKKMFBBwXcXS2HwQg9BANLLyK2Bwv4hMDXw1TAtwQJfGL22T01gOY4yKUmR8dDfP3J/rdCIICsKBUQsOgw1QihQcmEd0kAVBv8uwvpjPB4tsONiC4EycIwYEHmoTSgjwXcvmK5EAossv01VGcegOMJS+1s2h5lAfIG7ghAfQj4CIW+z/tokH0IHwjF+SeAi+CAynpTgABmkQFCoYUDD6MfWrYhhwUYD2+364DwoFaHtXFvcHJwQPeG8SC01AYUAxDH2XKUP5Gg7wDzQkADQvE2IdhsFiDgAAENQLP/NSJnQ9BATeTAPpHEi1R8Q9pnGHGxTWHwARP4ICwap7jqAUsEOlRECxUiwzocQAChGqIixGaBIyZxGBnQleI+8YEnRhEOE8iJDOMgtSyGAiRCmN4s/iJQRVFkgIZjhJgZc5SAoQXig3bA4vO0WAcuenGDJqMZBkIxgHlBYIe18CMgAqCIcAwBAiv0UwD04YEjBoACIBvfAUOxIyXcD3ZBakAlY6OB21WufQDMoAdA1kcXls9UaCkdHDoQxHEgoHOvEwoDgKm3B/RPcLPgAAMwGIIL6GOYZLKgEJT4gJO9w32EY1jVKmYqAPBqGBTYHSw+YI2iEUZsIrDAmIr3DgAYSBEzoxkISKm8iUVPhcMg1tnC1IgBPBGTcQhBB+g0jivtMjYIYBMoGmAN2w0jHBBIwEPF2DAUZSCeHhQTO9/xOyQmZHpkgqMIoAkLhjYMVw8woAiw/tmINA5hZ4powAN3oQEQlEgE1ITEyeLxPkaA5EQeCcFGEcBAOnRAKBJ4ysqoFaTHAMCV4ODbSWH4B5LGYgFSpRwkPqC8HDaAHZLEhAdUMwfr+Q6qjCjVH/B5r/5xKioFKJYSKGBNu4gqFhRlKTVE6M5YPmAD+pDAlCLQlSMwYTNUk5scDmANALQNFhfoEgV6uoh8ceAAgMpJix7lMDKpVAKQHIU0RfDLQR1MABgbihj8EoAEyNVKwNtbqBpwM3CIkGa6tE2mlJAJBExAAYXlBQIKNA+k3NSwqFFEAROyCnOEAn/LEQDIPBVWFoXJMA2YyBQMEADBHCA4F1qXAuwV/kJeOOVmHSQCBNDamhBW93lkC2gxybqI6CgoSgb4bv948bhFHOCJSvhfKFOrivEGVRYdIJGNfgNYGikAmCEQaRGwJ4wKAPg4BAjA/YYzgUoAAgKSi5AH5tEBprLom3Z0AIFVsRMBTMBIB4hxjI/EJAqw5L1RUmECZhrHDS+iOPAIkFqiIMA+PEazq7sKrz6xY0FAYA8CA9EgEAaxEIMBAebwFi7hIWWcngMptx3KsZrnAAY0K0pnsIABNhDc/IBuqUOrGQWjcoALxHALXWhRwJ6gAGmNhc0gqhl2ElcGvHDLNghiVQMKcAFGb2QB9kLQBHj8FcfulwwbOiJlHvHc/oplVjZdA4QEKLCnWfSJGI64dJcBUdPi3mYU4DDxqk1EAAeEVjh+wuyFZ20iCCjgApDGdXReAySaRZnXqiAABtiAHb8OOwG3WM5O1kxQZJcBJgxQAAYcMIEIJCABEZiAAzCgAKAc+WwKaEAiFpAPayeBS6GmmYBaRG+tvMTX6k4rkN1dhKR1YAIUoIskYSIAW9O3IBsotrt/s7QMfCACBjD3unaSBjbUdr4beC2y+1sxVuUBA1lAjwUs4AGLHOIDzXbIBd6EbIIdkQ4ZiLnMYx6QV9cBrLxGjo8PvBqb+6kDEcDxMgiwQmkg2ud1GIABKM2cDWga1+iwWJJXnbeDayN9GutQsLWTlhqo2/wAG6AA00EErQAo1uvROEAEKHBua8OEA+p++j8ycIAPOEDg/KaRsi9gJKQfYAB2Pm7eoyQBAlBgDQ3oXDcW0IAEgEABZha620FMgCi0JQDensAEAuAADmSBC2e2SxAAACH5BAgFAAAALAAAAABkAGQAAAb+QJFwSCwaj8ghACBBaBgegYJj4HAUFA+DgJAsk+CweEw2AiAWBShyWRweoRB8/shkFpcEiMOAAMqAgYJKEgITFwMdcQ9zco5wi3JwBxUXERR+g5qbhAwTFQcZcY+kjaaRc5QJHhKcrmQABAYNjKant6W5kgF/r75mGgEfGYyoxrjIj3AVBL/OQxAYw465ydakIRkGvc+uTBQVotXj17gZE63d3gwJ4uTv5Y8FGureHAOixfH7uR0e3PUCAUAwQRE1fnSI2alTq9q2gIMAMGhwDJ4cSgMabAjgwAoVDAE2NPgAik6EdBAFOiBmsVGHAgEMMOjypciSgQw4BCiw4IP+hZSaEGBQVO5BAwweENSEdYaBBYBAAUnER65DAgZekNzcCjXqL4JvSi2Y8NSMBA0WBFBxwBZDFQoMNGT16gsChwqPDqzqCoGBgwQXQOWjloGSRhACMtHlBMACrQcLEgOM5aBCB5ajTpEqXIHsXE1LIUpwMAHBEQkTGtJpOecDB6WDJBig0FVd6CIEPhhriSpDAwOmpVLoMKDZ4iMABAxGSC3OgQb/yiCoAOcChOPIQSznTcdlAA21i0hIMCdDAJTYoRVodGfAh/cDBiwghqtUAwbhhQAwMDjEAtrpESFRBQvoYYAWGiSIlgcc/DWAPuN8QI9WAixgX3AB6mcBAV7+hLfEWQJEsF0jBxiAXhEQXAAhHBlgkKEr0VBVXgFlaRXBOBkY9yJoCIBAXQgHBIAhchSM+MhJzjCRnxhnOKEBAoohx0ABHyiwpAgAcGDNAh78EssFFEglAQEcRJBIBwd0oGYFCXBAQJT6NXGlEBKoSE4E170CwXoVDJkEABooUABLjSxCxwY6asJAB8n4U9ucShgwB55hRDNNNZBAkkCemwCgHTm8GCEBB5yGQYCFkzwk5QUHSGLRAn5qQsCD48BqBANGRQdGihBy2RUAGISVaQYd9LRBBBFs8MECC3QA4Cv7EZoLBzYFIEcHBpRqkwJEPXJBrBJE0O0DB1wQgAf+fnDVlwAncgLBAOQ0ABAEP9YRAXhHIFBALg1oK4RdCzBywWuQeolBVYl60CodHwgAVXIjduCwVgQUAFzBSXxGBgLSFtpiLwBMsByLGMCm3wWlnNOuTb5IgAE6YswFQATJFJBOitiwuMGEIhCgGTNeAeABdVb+2aMA+hWJTHFCWADvLQ90wMAQAdwSalQIoBzCB4kKaMGgBfSS2zjOYqnAwrkUENy7KfMMlAP6IIkiB4z6Z1y4yDzgwBIgkDNBL4tis8FiFAi7wNRKECDyIiX+AWzHR0IQ7jgHcOD4SrYooASc3Uw+BwjcZG2L3B4ErEu/oldTAeLjnbKAjuGC4K/+l6U7UgGnohf6rRAI6Ab16xqYfssAwWmAsikJoOTzAxfgV0/rjnSpn7WmrE4nLbpkoAXapzSQjgV4mXK1CAo4MgDi3QBwtiMTKMHBdgcUPXMyWRjJyAa9CMB9NpYPQfO10lOHBsL3gAFwo3amUBXckIEFHIUqSyvikhKeFoI+BUQCurkWhsZmir0JQQGQm4MDFJA3F2HJAKUYwE/+VbcHfABjgvhf1FYoAgRQRHzp8ID9QrAHHBlAPw7ABteGgIBu4Q8iGIDEAQLIK1LATAQ6RMYEJIUpzWEJbsqQ0BA0IKz2QaR8kwgTnezUCLlZYH+NmCIyMkCtK2KjATzTQLf+vBgQDjjiAGIUgQT2hbzvodEREbCjOX7oRlNoUQhydEQCYBgIFE4igHUqhRl3OAEwVsOEwDqFCqHRQnlBhHoh6AANbXiKvwkhitUIAAUw9YAHkpAUEtRP+Co4O18A4HihxJAGPmALD4qgSOPAgABWZKhFfrBj8QNZAhwhtYAgAFUhGABKhlYKDPQCAzvkQwjjcIF0MEB4kvClCKhYB3E+QwO0ekAEQLY+9sgvNeOggAXAacji3ZAalGqaPGq5Dt9Z8YquQ9wtyZEUXJ6iAiuUwAY0yTMJ3NNZjCwDBD6BoT11Lzi9Q8brIECealRuehGk4X70sQF+vkICNRKBBVr+KInkCeGbyGiAUqqGqasJoIUuVAB6nHbHf0bFkYSxJpY48Ec4JC9ayCgp7wi4s4cFYDCejAoAfPcIUYJ0HKATAUyrcTj9pGYBJkLCLgFIFw/wKx27REYya4g9zWgDcAWYmFaK1KKVqWOPt2hjDTdgpGiucH7IsJl+TPanCKgtaMMpxQEwBKiq3cI6QygcOboWM5OSQQJ+wZdYKVKKrApIAdTpYOigeQtTLkYiIGDUAHR6mqeKBX1ea4ckDkBDLC0TGRVwmzec8CsEUMBM+uhAAPylUKvZ9QwOWNgHTjRMHHn2FcarQAOmOAUDHKJZtxCsTYZmivOFITkPMiER3pX+jE2edKEVUZ0DapkltL11DAi4lxkWWA2V2YM7VsEKGPAmB+2OITzgG0coA6gJnJWjAxFIDHzhtYDabgI1OzSKXW0igXadwRCWOYCGNbymBChAKRgTwGojioQA582cyCEAslIqoIGkhQICEIAFwENiQE1YECEjJh3GhxwB8NKF2SLxjni3zQdYMAkQcEDdLtIACtx4yGDgTy6W+CcLaK1Qy3BAH25jkyYJ2RmpIxEGJrxHcjCiXHuIC5QqDAEIPEEBDhCJrjKUHHpuIFYC8rGAE9KTBhSgABcI9LKEBdkdeYpQCL0SoBJQVN48wjxf/kWZyYW0y1JgGsxxa9F2NKv+VjKSACB4UKZTSFklRBoWCkiAbmFBACWPmj2bIlJYgXKTTklAUPPRMTnu0AAOnCgWNMvABTQL5e/KAjDhyNsd8oAB/QoIAQaglRyq9ORi62cgHlCAAUAQgAgkAFkTcAAGFJAUjdFJAQ1YGCSAJDtrl+EmbJZAuriMJQSgG23KqEMDCHDqgJxBLhFdggY8EAALCdgRYLVsgCAQgAZE4EA0AcMSEJATYQgrb3kxrbXVx5I7fMDhGMjChixgAQ9QwABsGLRm6uMSVVn7jPlmhB1mTnPVcOcR96n2cbKGjVFkxlWvZmYC8LwjCTzVFrXQ9asLmC13J21cPv950BMyAX5rO71pFOyOzacetQ3M2el1qk9FRn2ADTj56pFS+tTvmGDOXR0Cs7jMza1RmA9gAOBop5gDECGsuQNpABcAgbPz/t0xUeAvDZgPPKLWAD0owALmJvx/m0AAk3MAJN6OwAQ4woEsEADg/SZDEAAAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPyCEAIEFoGB6BgmPgcBQUD0ODgCyT4LB4TDYCIASOI3JZHB6hEHweymQWlwiIw0AAyoCBgkoSHhMXAxlyi3SNcg9zGRUNEQISg5iZREwMExUHio+Mo46kkB8QmqqBAAQGF3BxpLOOtXUUq7liABogH5CPtsK0ixuXushGEAaJo8TDwgsef8nJTBQVsdDP3BkO1NW6AAwJitvnzxca4cgAHM3o8aQZFODsmQAIEx2y8o0PGQAKhNQogb17g8Y16Mft34EFHxoUCIChChUMATZc+OCmAgGEmgAIGNBwVocLE/ggkPClyBIJECxwEHAQZBkACiqUbNQAg/4HLzXBtLTJSkGHeA86JGDAEsmSp0/DDc01roA5bgsmWKjJRIMFAVQciK1ogMKWpqtcHROHAINOYQcSeFg7pJCDBBc+mYtV58CkBCAEeMkkYcIBDHSpWihXa4HgoAQWhGIoqlEGv1rR3hTAr4OBoKoAePgQUM6BCQjASAhAENiwyiE+cNAA+giCBnM61GPXa8GDBQZShbHwgeE2RgAbGEi9y0GjARbuSWAQYNoYABje7HQW50AD62AQ8KMT4oJwqVPDSFjoL1icpAFoI5GwoVaGAImJJrHA706FDwB+MIBkri3ynoEPNMAAVwpo58gCu+kXBgAGgGCABxYQoMGGUP6oEUE2cwgzgAIHafALMRcwJ6FQ6SnBBAEURFCaMJ6BQ+FVtTxgADu81LYKBBjAM88CDBBBwC/DZPCRNQI0sOQ9+YCQzSgZRACeECId9UwE+eHDQAX3dTkhBE5wMZhTDBTg2gAcnLeJc8NIIw4BuIXgmI9YSqCBAgkM0EEHBwDawQAJzHbEGQ78GV9tEhTwzAMRuJkJBI7O0YCKh+5pFS2RYADaOAuKwV+cV2JCoSOeIrEMaSWlmIsEIOBISgB4ihEZKRWs4xIDF8zIjW61kkHAAMN0gKkgEMDiyAECFAGBAw4y0sF/G0QQwQYNVDBBsGRQeMAzHODTYC3bEqGPNv5yZHBBdX5AxYSYk5IkzAX2tIgEApWOcmkRGlzASAYNcOAHJggcuwsGxIRwwJMieJAAAXgCQIGsv9HkEgIgHNUBBmcOUtgGBoeBgK+WpSoCADIeEADESQDgr2X4OWVBBEWqwgEkBgGC8jAFHJPsew0oICYBpSzA8KGrIHBiBhxwe7IAFC8ywJIWyAuHUiwXAYIptBJlADCxHT0csbTodrICpTFygAMdSzAlHRmEHA4Cb0EyAbwnE8BcYc98A8DW/wB0QagNj8fIBitSoJ2dNZuhgQPA/XGjMFxKEMEs3X12sgMkh6DAipaLAkJNEijQQEARUOPBAsQ0QCYstBCJZf4CpBi9omiswzGAm/kkYLirIpjIqUcavE3KB8z1S0qkQyDwGEISJAC2B0pwhm4FNa9nixxafGtLA8d86cjoQzSoldNFef/ABEpwIOsBJJ4cQYEhPpAF/YdTI4D3i2SguRDzk0TT7lG89wwAHKsjxf8wEDU5KEAAz+iaO2QlJyyRRA67k86JkqKiI5HiG0JQAP5igQEFECMDGMCSAUgBnbpoKTboC8T85NCB6AgBAnWiQ8wa1rk5BIADz9jR5hzxgSdpYDwP2EAMAYGBWByAekKQwMsYwSUhWKCHcpjA12jBNCw5ZxQf0FXwXsg+hJgQEgfARRRhR4cEHMMC/HNEBP5WKIzPbY4UDRDjERdRrnsAsTtqFIEU/1FFEVyRckDMUReHCEY9Gi5n9/gaHJg1BEo5o5AMwOL6zsidB6TwZE0cRQujaLgGLLEMAXhPDZuXQz4ewwOaDAAFOHUfyd1sFBUUpG/kUAFJtYONxhqCicZHDQqMUA4OEMAxHwBJqDUCftQAAO2u1rhqYGeXFViLB97CiP9JkhZ8iOMoLhC+XYYIhELYIkBMJhUGbCAu0WwQlQYIgAlEDQ4UuJUtkCcEDbCHEeSzIiN6hpAzKKBxAPgiHWR3Mn9xygO3iYYN6ROJAVCALutZBLBAMhQIbACPzFEajQgAgcvlqAPhOlkAFP7RgQmIUQnfTKIvEcIfUrhRCAwwJ0gBwBrMPaBrIuBMASyBhKotAn4rWiGVTEan7SXACwbQ5AaEAwEKdMwMK13EAuRmTbIt1IYnQ8BK6fcA8uU0GtUUwzAfAVSQKNMR4NtVAeKYgfhF1Kf/u87E0gVFkDRqFilVBjOkZsOd5SgExtCZ9EKQOlVIYDafMgopDsBVAMzsDeYZguJ8+gCxiawAYdTEdE4HnKsK4TazCGjLGPABdIoAArmjRVvJ8IRTnqwTrCsGWKPImgelNQkw2cTliLGAl3aLRWZAQIyIRR4FFYEChgvR3TIBQWF4w7aHEgBKDDAFAwSgDUf5xwWydv5anc5hlJiAgFdnMYDfZkJ45DkQcioAgvzQzRH+w+4QhfFTvJUBVhSrRQc2wBQk3KwR5FyFBbhZiwO4VxnyQQIDtFSsSsy0kvn6zW4JM4FlhiCuSYCAADgSHCQQwBOAOoCKVTytBFCgXRPKKQ0HmIsFP8oBXbJsBLSTgQRs5VD5sIAHKEABAVgAxv+NlTf8q7Oe0ihClWSGa04RHP2q9QMBuPB7xfmPD+wWAb1C13vUFT8oWUDLmojqo9oqAQxEFzYPqIAD+mAvF61IZGw0icXaZ15nPOAABdgDA0gKAQnABAJPmIKVxSGA2P5jui4Z8U7u8IELXKAAlt6IGxx8Z/7VxKoWRXSKBhLAZVMg6NQfRrMZoJfhR9DYKSO+54Ggcd3rIMAAP2ZH1RDMZBEQAATMNfVx2hsxC9TnAxzjEXTn8MSbEABy26lFIZ2igGYo7AK5toYB3rBkVlS1DdE6B0D66hICzNBAClvZoovQZoB5tluuwAuBrLuABiTAAcYNq5TJisE2VUMCDni1qfLBAAVgwAETiEACrDUBC6lEM3Ux3VX8TEMCr9vO7XiJob3AkhYtAQEK+MAbjlOHPnYaPc7LGHmgAZAJqPrkJ3vCSnz0cQZwYAINKDWtswxzMVgOIpQoIYYIYAEhCwAjGwFTKPzRAbb1fEIHfoQdpj51gY5QPB4fKNXTbcNgYaOrPUlJAFe3HsUOv+br0X5EBUpM9pYlEOw7aTl5226bKcIdGh0oAOHo7hQG3v1XG7go38OgAUenfRgHiIBVL26Teh7+UR1AtgYgPvj5vELkj2f2AC4AAgv0uvKEsIAC7tIGWYeoA/YGgQLOzHiyd4UAQ1ZDACaQgAQ0POBZOHLHJRQEACH5BAgFAAAALAAAAABkAGQAAAb+QJFwSCwaj8ghACBBaBgegYJD5SgoHgYBIVkmv+CweGwEQAgcR+RSOTxC8Lc8k1lcEiAOgszv+4tMHmsDGSFyhoaHb4kPbwcWf5GSSEwME22NiYiHm52IGxKTopIABAYXioidqawhBx6jsXwAGiAfma25q4gJobK/XxAGhLu6xiEdAgDAzERMFBWLx9Obvc3XAAwJB3HF3sYPHbDXzAAcFYXf6roh1uSyAAgTHd3Uio0PGfn7+ATv8Awa1Fu36cCCDw02BHBggIoBBwE2NAiw7J8oAAIGEEzVoUAAAwy4eAG0pOIkAB58lVNQYeOmBhg8IBhpUQiBChhUypJggJ7+vQ4JGHShVLLozgT5AkD4BcAAN2oLJlgwSUSCBgsCODx0wLUhBQYahv5B2UFOAJ2kFPhcdyBBSkqn2hTiFK5CQhACIFAVIyHCXEM5R3nQOG1B3r1FBMAZmKlbhgMVpIoFk40eow4GEPeRQOHD02IHJuwJAwBVrnuIGujRXHWDqjgdFLDmg8BAg7+HFhhYKgbj51SeDmVoYGA0Eg/6GCEaAGkULQ6EEVXwMPsIggL2Vh1owEAzAOyqFF3g7bw2oQcNpvYBQGFt9kUdAmjYK8D9qgwg0IqCgGECgepJSJDAAgN8YOAHAyygDzUXUKeEA7jsEsICFAC4nlhMWEjEE1r+aOChBR6kkcAAEbYygAIqIZDAOg0YV44ASsWyhAQECOCXMZiZpMEF6mTgADYKcKPANfwRw0ojIJhkwS3GaPDiUx/oJyMCIEQjYQaBiYDRAt88EIGUkWRjZT6ykWGGExogoFcS2WDXSgHGAYBBiZ0sMI5zBDCZyAUulkGjAhEM0MGgBww6QIxHmOFAWYhkUMFuVEmAyjdfxgIBeIcckFmiGnCwAZ24TFDdlvl8gMGaRjDAaCt2aviFnLjxQt4Q/H0QK10d+EOZABuAMB+bEOpCkSgEcLlKBxxQlc0FC6ojam+ofpHnNx302celdB6QbFUOuHFsBR8UEEEEG1zgJJv+fjSVXCscpGWZIs8OIc99FwQgU1E0JerBr3xAQKI3DbhqBAKTKtIiEQjc2OgFq/nh7wbnmmkAtbquR8GtyZgEgFqHdIDBTGMZoM8G1oKhwW+dYCmwEjwqksEEaAFgQQIdZMDcJDfFkcDKAESgSwFgSgvcAhUXIYEAIKgXZpCNbjsGRrcud64E0X4BwioPDEvUytaZBscHRYdBwL+sdECBlgos4GvQIkgwZqMR/4PAAnJ4ybbRPhfzQJIAXP1ABUmz5oF9D2xQ0xACePvAAgyYicG6q3zZ1yI2O8Dvg5C/MeThIkAQwSZJUjYEA1y2wifB4dl8qsYrKkI051p6QDf+HBXMCggDxQmhwQfeEK1BS0OXKQICAiniDucC4iLAERAI8nfjWjZwWhYoH9KALwwAD7pJk73D9BsT3E4zHAdUqGXeRwrgwWkbVJR4yk6LgMFuFv2eyAcmQRBA6YlsKoIDUXsDByiQi5ctwxyxspMSbDUADtgOGAC4BRyqNYTdrcIBFVEAqACjgG9gQAhNWcXNhCCBtUSAa3yYACIOUDHieSJeyMnFRwpoABBCqBNgq+Ba2vcODCTiANDrXMESUSkRMCBzm5iAAVphiM0BwAGr+EDENLCW8L2DA3Eo3xAkUABPuMMCSJRDBJZYjAy0S0tQVIQUdYgIK5KDA5k4wPL+SDjERkSAN2DMxRhPc8YnpmKNulvVzt4xMUO8glZDhEMR81iMCXSwFVgC4cQUMYCKQWBVAXvHDZERRAhIbxMPgJkQYliMAFDgG8NqSokUCMLZ/e2Bv4CAAxREQd3xjhOhE8HFvIEBAWzwAYMUgQAgl4ECNEdLrePkP+IBggKYhHT38N/jvMGBI+biAthzpakuJwIy5gODFjFDRTaWuQyUCQAT+KUCipWLD4xGAwJZAAa4KQQLbKIAsHyHH+s0DnXlQiafZEUFmiOBhXSvKsVDhvmQV8cQmEsJ8qCbIjpgAc954wBOPCggvNmOfF5DVas4HggJEAFXhqBFAAjAabT+JoYlrXBzh+PoG1Smr3kYohf+LMYGPGqEguJiASVrBgDIhogFHNNPM/PRMqBZDMbxYXd1MxcKAQGG9a3ieqSxwH86F9Dg+K83uzQETSUhAQV0Bwlc3MUZZ4G+XTiTD5MzRGymSsJ5LEA+e2EP4Q4QVDBQoHqJyMBRxYCATz0Am5GQQEDmsID4DS+hdWMpbfjHCThIljQEKMAh/SABC0yAsg/4wFnrGqsJDdZMyUQNUNdDAHCaCQE2IpEaRyuE96XsLJLwpYTykUszVYcnGFCAAjAQgAsswA0uu8BWh1CsXYwwEhC4ZTEGEEQ25esLBDzSJuoAglnJTLrC+epYgqX+N9wGCEYOAoMEIBseRHRgA7QloUrdejcxMCA6ZbuT+MoyHQANk1oTyEuigpQKo8ZCAhMIIBywWhVtIBcOuqlaVSLQgUI9plAdqEAEKCCSANURWXRNlUnv8QAHqEQCIABsBhKgtDIgACsUoIAALDAfVw1zDiZmynyB8wazjXOaR/oApLDRshAg6hcn024jGlAxFH/jABdA0TV0K9JfiEwdByMhBgi3GENUwAEM0EuIjTaBIzODYMcg2RY5MGJNyOEABciDBwgAAQhIgGoQSJPAzECeMSdKAJQtpUowAt5vOAohBbhAoi/wgTsm1gFSZgoIwhjY3mpJA9sgyEWF16/+BG/nMDtp6CJCUIGSscdWLknFQ1+b2sNSQMKRsAB+QenaI2jAAdF4j1gd+yoCuIYuB9iAAnj6tPawYrVf6OyWdR3a+qLNSMHpwAXiGzLF9U9DZqCAca19DE5zKm9sEe8fJADATeRwFgTAwBoU9Ms6xMvFw7DHBIg9BgTjeKpMQAADFGAAEAQgAglIQAQmAIKGhEQzZW0ANzaSAHqTAQIJDsEA4hamGd354iVJFAIUoPBUJ6CviQUBsmCnJat4YH+MMfS8seFA54wqHgzgwAQa8C6XZMDM5XCOIByABa1aAEQUMMAaPnDcdKQaGVki+WZIRIemH6Dp+dDE0ZecXqV8zwKOi2GiNI4OFJBbPQnRrdvUu9RAh399o5Rm9nb942fYEYC9ap9oAah99qc9Lu67eC8FnF136xAV7w9oC6j7/gcgj32mBxjAxzRK+DFAwFjMTvwFksb3xoNdDTT/5SY60AA8KKCibbd8yV/MgKBDBOADD8D8es4Fxl8jCAAh+QQIBQAAACwAAAAAZABkAAAG/kCRcEgsGo/IIQAgQWgYHoGCYzBwFBQPgwBJer/gsDgJgFgUoMilcniEQu7443GoKMb4vJ4s8UzWB3CCcYNvbiEWe4qLYEwME2wZhYSUhQUAjJmaIgAEBheEk6KVGQqYm6h4ABoOH3OjsJUNGqm1YRAYrq+VvLEBp7bBREwUFaG9yJMVicLNnAwJksnTkw8JEs7CABwDu7HflBkCmsDZnAgTHW/gsQ8Z7vAD2IwQIATl2gwN69SUdAsfGmyYgMGKAgMYGGgS8GCAgHnaPAxgV6nDhQAGGCCQsCQYgA1vOnCAWAuAggoUBzXA4AFBx2wEOrzqgIHkJgkG2lB8sCAB/gOOSJYIFcoIQIBwE2wWzdkPzoIIFvAR6XPQgdWCBigw0AAUjwZXkwIo3WNSJ7UDEQRILYIAVKEQGeoIBCEAwloj26QNcudgrCoBxsA9qEDBbqMAeivBiVthws+7QiRs6BXiAAfIjRhMPDsBgVd10+R84OASiYd3ox50MKUIQYN+Cwz4JTP5myA4DxoY8GwkQjI4y8gGYFfBw2wvDFPGOdCAQbmPgi900cNqQCBeDy5EVYIHwr6mh0IG0ABMw+TfGcSSlUChwILEGS7cE9LJwPQwJj/oHzDgvTd/hFzgATAI+DbNAhRg9kUfAXwgSAPzPdPAAh7gIQEBGmhAgAUe/nDgQATGKEbJAApAhEBtlGVHS1EaGPDBAM7RJ0A36R3XCBMaUBABasjQVN4F4GCAykucKBDIG8GlgssAPO7ljnpCWPBaLBkQYAsAAlwXhwEK5gEAAiAEFoohGJyCpUyxRGCjKgyIKcgHXXICgRMYdnUEAAwUkNguq50iAQb/FULhkASA5Y9Cd0qggQIRDNBBBwc8ugAzQUHgAGiG4Facn24ho+YmEBQwSgcVFrGKAgW8slcI0jUiwAK8dAClCAyg2QuFcXoBAAaJCbLAZUVA4GKTx3CJ34yDPKVAafSB0Cslv2RCwAKZxiESSXhegFpqA1DqKqwVgGCBnUMQ0A0s/gvwpkiogVpWjgQOXDdIBh0AVMAEERiLBATLDsMhuabmFAsHi5ikziQTlIPAcIUccMEEHtiFCWYAUNDBBuTlAYGDyDSQqxEIiCqLuiIU6E0GDZD2sQgSGLiBlapgEEsHMHtJAbEhLKDWEBJM0OQBu60cJUpzbEByGBrIS0qZegDQqSA1EqFAYt0qYlJiCawMgG+wFLCmEQQEukDNUTqgbXbbtbYBjxkAKwaWz75JthggVBOtqQhQIK7QpmoAyiEfzP2FucgcQIHGbrqRwdHDEMkIArDi9qkYLSPzgAMre2CrIBuYQ0SWhyyAKH68evo1fQ7gfIfnQkAQAW4hgPAx/gPU9tLARhYY5oUECUwyNuv0eVB7CAPcB8ZX6Gq4gKwM6A7ylIREYLznvOs1DuXQT5KBFjp1kIDKRlhA9CCyA0/f1HFMoCsE82ydjAAeeMOcAxkPkWUhGehrvgYowQHnncL6AGtEkDpYcEAAoohLAlaUF0GV6hwA8whYQtABkjkiHZLAnBAUEKg4YEABsMiAA+hjgGoRj1ISGMAEBMCsZkwANweoGZZSdYgHJEwIp0FGADgQCwOgrhKBGwIC1HGAAlyvGYB6wwFGB4GnuSEpQmBA3J5Ywl607YeFmMUQNGCrGzaDh3M4wBEl4MQQTM4COEufAZBxRU44YBIfWJEI/pIWhwhkA4RKPFxkyniNKKURDhEAYwJ96EYgynGIgsiaMxjwgXeIcQhN9McZ/2hDECJDSJxIYhwW4ID7QABTHnNGJzhQgA5QyjvHgFIORxEAClguAPThwBwyMAAMbGQY46vA9DwigXFtkWOwK98cG/AOXjggfqlRpAgE8IENPOROvXOD6Mw3BNoVCxgA8ECjesUBKVLmAvMww3GqCBcNmq8s+BugEggAgrPBgQLTQsYHGJcECwyiALvMBgBAUIlB3SlvCXBHS7JHiSTd4jsUTJD5JCAyldBzGC1CAAQCKorKrA4/a4zDBvLZjFpNoo9vO0pq7hYGC2zGouYjZxwy/sC0txmAkhsdgwSOssmHNk1X5yLENPFgzVHsVAwaQKiK+BYsBTRvLQwQRQNOJ0SC7kV/jbgZ1CJg0zBIAAQH6AlpisBQURDMSwYaxQaYyonXrdSceoBAOtbRAWEWySyCOEBVk0ABpU1CcF8IGY/UOQY8EdMQDwjiFoHkD5LiAXK+0JoGaHi7PPQyANQC7AcGxDOaCspbqohmRdPlJQLoya1f+JKOcvqmGNkProqblR6Sgx0RUswJ2ESAA0aHFwJMwQABaEAkJiGfcsRzElVbxMZgcUKpfEkAO9LZx9h1G15UoJP+AiYh8kdUJTigg7hRbX0KoCWHxAlPm6tIAihL/gQp9cJrmhBfLJaoBA40MlPAIa+uLFuRFe4SAYYqhEEZ0TPsumGpwQvRW2LDUUh+4AAIjlQHKpAACrhkLRqI2wH4ygj1Wq4v50Mt1BKQtjtZQAACoIAALLARBQGgocvB8JAmMI0OKJQTa0zNB+xT3aBIlRABKHBrKAkHCFU2RQ4rUS3wSwiQcqLGRXjpNxobGQxoODyDma3EMgGA6z4JImpNAAuRXLIy+iMB6pIAB8bnDxheAATdJIBEJSAB54WhTW29T3/hUAAMEICspnoVeqiqhBlRZi8ZAEgDCnABQkN1QSAQcmScNa8BQAXPSmD0NMCsBA3sCDupEcQA5spV/mCotUmA7UADFOBmMTDXcqx6FwXee4yKEuKrXkKAZuUwicnlgQCJe8tgpKIBEHQDPD0WGgAsABIRDYJUVlNAeCeBydpeCtgHwGxoFSDdEKK1aRx4MnDwqgR+XWAB2qZEBL5LAK5RI2r8LSAvoNgIAmBADf7BTnDJgAADkHYaE9CxTFk8iQ7s7G1fYoACMOAAfCUgARGYgAN2szsFNMCulkuAvrvjs0K0iixLYLPGOeI4+pSBAw9XDqVrcVWztFGfx322cjKQb20YADQV4LTVvmQBDvxhilQCwcQXIWaUgHYPEPCABTbEoYP84QPgNmFTaAJpL1lAPlRGTAamTvU5hZQZPLmRrzkimAcEmGUXwI6V0ahJDiXXUDmjGAAHdk72LzQR7dNgeYTaXjAjhT01FzMt3flrbrgfewOF2fsmLBC5uy8nAoXhsuALafi4fIB+XF+8HiRAZnYcYABn1rvkN3EawSygAQlIdO4Uv/lgeYACHgpABBIwgQkEwAFX8ICap8y6IAAAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPyCEAIEFoGB6BgmPgcBSMpHbL7Xq7AIhFAYpcFodHKKRuR77wuBwsEUwug8760Wb71R5zgoNwTAwTFQcZa3+NfRUIhJKTRQAEBg18fXyOjhMAlKGDABoBHxmcjZ2bGQqgorBfEBinfqu3ah8EsbxaTBQVi7jDfBEShGGvvb4MCcLEwwccynMEEQLUy5UcA4ucm9CPGpIGIQsOENqVCBN6tuDwjg+oGRvZcQCZ8wUa97EADBrsCffnQIUBDTYEcGBlHCEEatg8GIBNGwABAyQSfNDhQgADDBBIWCJKgaoOHI7xAqCgwjtiDxo48ACB5MoL8jpgUClKgv6BNOE4JmAwMsmSozbjEFgAjk2GCTwnAfhJcMEEC/6IWMLgwAGGKhQYaCjKZSqqVQ8CRB2lAGi8PgcSeMhaiYGeiBkMJgQhoKYWCQmGZQCxVs5Fl2//LOhL1wgCgX3+5K1wlWwRAhkTczTQeAvAzNEmRMIXwJsqTWw+cECQTUCHb7c6uBL0GNoCA4W9eHgtb6CaDA0MjBbC8oNmNhUszAEwwfZcJaxlgY4c70CD58QJQMb14EK6Qhzu4rqAlbgEDAt2eQEQYOOaDgH6DUGwAXbTDGq/AESMqwCBV5bUFwIGcFjwwQADLIDKcWo0wIAyCERgnyMLUNDZEIBNuAl5AP4y8MEeDeSGBABOWOABBw4kMICGf0ykAE/0JcZGA8NtUUcEblFYnggXdePHAsotx4QGAkRgWlMcYaCMBpmgFUIGBMIhgQcR5PEIdiy548dgF3Yxi4/wqBGAMtrFs6V6hSCgQAQNHHAAZ8S55hsbG3wnFQIgVPDWA0rGKV6YxghCohTf7TenGhVk4QsETmhg54gMFHCkLSi9ch6LaywQyCgAajepGgdM8Kh5BHBQZQcdHHBbZ2E48OcfFWAnwQV7PhDBqIJAsMGhIVRaiQYKSArOAoqWJQBTnXzgkAh2mdkrdqMYEM8C0xQBgQG1JIurVlphtEoEloLwqUZjSkIAb/6b+KoEAxeM20a5SBBgYbf8wVUsZmY+sECNckBQADzSUCOBAwfktMAHG0SwrBHsZRBBjQDs1kYHHwSwI48GnBUPB2y9qsYnRLDjyAEe0XTUX4glIB9xBlRwwQQM+GXtdI400OURCBTQCY0hG9liA6vdLIIFET2wAZoiTCnzEQBggEsHSOtHwacdVCSEBBMcqVN0cDiAmtH8coFAwfFAKbQSOLECMnEOtDhAsXFo0K5EDyRwNgAS3lKAiFoQ0FR6RDBANhsXPDiIBs7skUG160391gBRdxEAPPDyGNBZA1y8HAK0RqQLHAR8GE8HFMQBAc3zLMwjKRwUoPkgCNQbqP4XEDT3lgNn2+XIBr7AIoBbxOqHweCb2LrtiA5o7IcC6lgbwR8gCO3BArc0cPwRgFEYuUXTR1TB9Y6J3skCqmshtyMJ8N1Lhpxs2oUE2xWfgftcWFAvG5U3T1xbe0ygX95NCUHpvECBHM2Dcfobggb4MwCheS1MD1BAqQhgGYYpYFj0S2DSxLcvzwxBAcqLyBowYJIDbMAAJmNYOTaROQ0WAYAdCBLDIMCAHQngU5wAAQc08YAKFMABRFEGANq2ic8JAQIC4Jo6nAaqDCaNAQ4owALyw6yvvWMC5WhEBg6GDraBQ1kKPEgCBKC+SeyQDQcYoBAQ4IAPvEYNs7MA8f4agcW3LI5tflDVBeYlhHP172xxGFgDKkA6DNFKI3VTCQQikIgJRYADuOAYj5I3gARggIJF0ABvQmA3dRjikkOAwCH7ABXoKGACHyDeBEwSJigRhwEcsEAFQ3mXmABSUMoQpScKQwoKBGBFIQgABTqxB3glhWl6WsP3XKhA4zQCdyMKgwcCoAABaIgPnQRDYNTQAbhp0AP341OXABLCP1ygjESQllOg6UKWzPEAs+mCBZDVlA+ArwgW+EMB7vkPrzUieF7gHFqS8wX4+YF0txyEBHS2CZ7RLgFmkoZ+pNWGOrnQAujyQ/r0Mzm0RO8LFgANPF24Qsn0aT0Ze4tFvf4ggdL8KGy5QmFNsgEA8U1Mhl5gAD1VAVAvaMCZfLjAygZRu0VM0WpC8MDO0FmEx+wJTo0zjcPKJyUQFGwP7EzavxwhyUJISKVMVcLz2pCBAsDUS+3QiHeUoACPHeCsWyggLrZnvvq04QEFAKQE2CXCmCDtfMXL3xcQQL23rG1EIlECASTlBwNIyQIBoN4fPgCtlsIDSILa5ioqQFUSUcAMUOXRYttA13V8Fph3dRA1fke5sB7BmrfgkhJuZKVnCXGRMeEbSzAQgAYsIKNtuMB/iLAUeLyNEKdLTAvXyB+NLEZgV6CLBzRTgS4qwUCdyEBol/NAtFCROad5wAeQuv46o9iuEx2QSzbK1Im9TcJ+T3MfAoD6jljdTAKo69UE+mIEzAhjE5idBNbctYYQEccDqEVkBwzATxEIIBEHQFUFEkAB1tD0wbyCZ0KPAN89OUAl7qSOUxLwOiMQiQICsIBI6CLQCR3gw6+M2YaZwyJu8nEqaBEvgzdshAx1IgCFYsAAKrABB8hyDggoZ0PRZFlV7IFkL6IEADgwx0TOx65sWEBwhuqFjA2jActqmgFFiBwgzhR2Oz0AkEOpWdSkBgNECego0bKB4Uw5nPY5QAF0KBYEQGCWvtgVqKybtAkgqWgONdZO7/MwJWAESY7Y4gcaUIACOFELrKwAAluq5P6CbNcz4tJM3eysASMxqBH2GCzJLlZUUW9USgz98nCvRgHjnBpRVIWUnUjU5urBtW/5BccFyEsAB6zo1BpeDgEEPYwOXBoMbWUQQrVSbOCiZXaFUECC7XjS5fzkOGm8RxgocAYDNsKI6yn1qQfjWiQMrMapyXV2DJCAMyxIMvH0DAIMsG1ciIoSWNMMto1CIgYogCsTiEACEhABaCVBAgpoE6/gkYBfBzJrqwhYIZYggY53vDEk4kCbbg22WEjAqvCAxDKWQCQQLJoYT2mwHHzisVSLYgkIgOUEJO6ewch8ORy4nxoFRQAoUMAAZvgAGv67EZ20ezkW6NyTfo4EDo1k4OoLEjHJr8Nj0wXgNQmQxKzck5j0WpwXEqg18wgxz1tzZwAMZiZ0nl6JlJK9RU+ZtdxDoSu3w6MDG3D43inR4bvHhQJ0H7xnduh3NF5DiYoPRfZunZcPYGAsXY88EjQJjQMM4AIgiLPml3FBtHSgAQkAgQIssLTR92KaE2D4BBbCAQp4gACYz/wXggAAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPyCEAIEFoGB6BgqMAJK/YrHbLFQEgBI4jclkcHqHQo9Ftu99apmdyGWTUeDTaAe/720wMExUHd2p6eWoCf4yNRgAEBhdoaYmIehUEjpt/ABogHw96opaWGxB+S5x/EAZ2iaWXagFWcAAKAQirbkwUFZSywYkHHLVvEhEPHRwIxrtXAAwJd8LVeRUWfhADaRkFAs7Pjxyv1uYhDah9BKRoCw4S4o8IEx2V59UZEeFtCnmi3gjwWxWtwb1YsR4cWPChQQEQ2foAiIAnjZ4B4OQBEMANX6IOFyZwYIBAgqpUFf7lWRZv162UHvU0wOABwklHGg4gVAZvlf4EAx1iKkvAwGSWJTe1KKCGMEMAdY0AGDizM8+CCRYGIhHgwAAHCgw0GE0ysaqoBwFaduIQ1GyIAwk8qN3ioNuBCg02gBBg84iEC27VYNDKBYAHmGYX8CWcREBFPRnuYh0rJFooc6I6GGCMJVrHqgcmQPXCmQiBBcEQfeCgwZkEEEGF3etQBQ4Cg+cWGBgtQQGD0kIgbCj1L0MDA7qG9A6VEM8ATbwmuK3gwRikCx1AAPcSoGqlAw2qK7GAu+qF0XGmYm6QtbIEB6gfnNpyKwNmix0CtB6CYEO7WE7N1RkDn1VTgECVEVAAUwtowIUFBSxgn2ykqNHAb/wlY84CFP5sB0Ey5iHoBQMfWOJBYQQIIEYCA/wnzAC1CdGfNWpckFwWFDAlzALteSHAL7JgwAsTKUZg306aGaNBedUYoAUEiO0onhcKUHXJAxdslwQEDtiRmijaDUHAZQllAB1ZGOhoyQGbVSaAPbKkMYCWZGngQJSPZYBBSxu1VU0EAhYhVTmWPDABnwRaZEkDGSHxhROlGVaAi5UsUwsADlCKyAInXiEBGBQEQMiEISyQHCQNaBpaM4JKoIECEQzQQQcbAMeEA34eokYFjUpQwE4PRICepxoIQMcHiwT3azAsCfrqgoo+AAI0gnqA2mNoNOCgEAzkKgundHoBwX5eGLBjMf5FSIBBA4ZcQgFZCrCqBEex7FMZCGpeQssfp5XSrBIMXJCvGh1E5BdeAsy1EZ5oHJCsCBaUKFsHN74BwSSyEOOaA0de0gFDDgw7hAdwTlCxYd6q8cG2Uun0JQcSVRnMBM7QY0kGFwRQU1KCpklKAuSW2zEeetaCQIvEYRkuf8sm0kDF9OhoHDO2JoCIN9sGl8BKF4AwJQAYIPTWmV0AkKMlihkjwQQ6doBBX/S92U4GG9wIQMQf7EWATTUPfUnRvAD2d1pK1DXKAAzAoYHA9yRgHc+CgihMAYFmwQ4s2BDBgMs19viGBtO0E2McAgyMxnNuBBDMvsFN4pznbwCAgP7raHxAdhYWIM3su11sc3PWL03YAYaMIBDloVysvZMDdJIsywbpWoBBBA3AvIkAVi6QOH0+11u5oxzLooCjJXHyYR5hbsHAAgilk/zWlyxwuzxeWEtJBSIjoYHEhWbCxW2yEBb9jCABq5GiU1qQQKqShsAsMIBhrBugEqqEhwAURnKF4p0WKGAlNWQAXRIkAgLYdzotcQwh44uDAlzEqRCmiznKyN8RclQNPqTHErZzYREkV7D/TaABA+ggGqYVB8NlYAAJcEDCdEgEBUjoLQ08igQIoAAQbEB3E6APBwpAE7FQholeIAAFMLCBhwFiXALAQAF0FgAKWAACXyQCZ/4Q4ADflM+F4VjCFNOogO8JigMEW6MBSBIuBJwBPCBkouyqeEU0AGoL/rgEXhJggKxlAQIkVBoYRfAhl+kBeRtMyAGYRx/E4G+T+3tMCEipBQFQyiKOo49/CLY9Jh4mWiEYjPo4N4oa+dEIgERDBliJx6X8bXRY6JcwPlAxLFgAEZRTZKYS0cItADAWmduCAg/RgQ7pEAJN08MFLImF8wnjACmMQ5osMp9NIAUJBIBTRRLwS0GpLiERxJ3u0MkJCyRRAPIaggFupkv6GMBveWhnAlVHCVM54mIEe4d1+KeHHnZhfdXQXheWlIdxLi1dE0AEdYjggVI0oJ5GuGacHv7QprjNLQLNbAMEQMBLYvqqFNYrm4YKFYIN1NMZZYFMAWK6BQjUQ1HnmaA89XAAouKIl9i6HRPAIAAQEMUYMxoFdegkAQawyyIZyOEQOCqL9MmUhLFgHSQ4EIALWEmhIlBQu0KQ1DgwIABtOcQHpiQCCTCUmrUEBEUQ0qDKKCBlgEvQpA5Bz84ggAKxksVenYG9maG0MTsZptGYxE2+yg5EiX2EBwLQgFFZ4gKwIwDDHjAAg1lMd6VA3MiWahFeuYYDH7DoEWaX0ZDJkQCwpUQGWmqLTFUDLXwKKSxCgBGgEsC38EyZQq5ahIhp6gJ8iuMWLMCwPByglmRVifbySP4YVw5jAnwxwph2ZDANbICSBNBuEtamKTyctDIPTI1mZGgEBxxgVhVIAAWaEQ4JCACtHgPhRACykA1wwAIl4Qx3gdUTKtEWMgmAXRIsIAAIy1cE9JBuCIbJpxXicsQLSIAC+AuAe1ajm7VQ13E/sJuPRq6+aFHH3ShaKJ9iISfA+oDBWiy2A1ygj4zA4CUaK6PF5iOKSDgosMapBAxc+DEVcAAhP0qBYDgFKhCA305opgXeik0+N5IAB651JUUd4CEj2ZuWCKCm7KgFAt0xB+pIh+B62U0APObpAzLAkAIEFgu3uUYi/Wo6PGgsefg67qATYDfQQRURhzjLA4iJBf76jhi1xsAzQq/E5OSFc7nZEpEXKPCBRldCDY9MzxF3owQEiNkaT3tD7o57iAs0SggEAEGLvkSJlT3IAZ6DxHDO0QEo04cCIn6M2xT23D4Dw4POdhRQFTBs0BQ0duoxC5Zg9wUKlEGIeMhp2QiwU3ME6A/qavQlbHQESEyvDKR6QAIAgQBXxEQ0jfD0OdznqCYwQAEYcMAEImDWLPTmqx5JAH9l+tfj2usoepQASpeAAAU0wJPiprRP/JvZdDqC41W1h1sgY7Jn/OTKDZtf7GTHAA78EOQe+bI81MzmRFTgsltK47HMMNeVh8BtE2fEdfK1bz+wZdC6wiU+wmNjN40Ytc8m5wVFMB0TPHQAaGCUAAUgngHXWn2BXb/EAGi9SdkZdwBJvwJ3056HDExA1ZsEdgQqHLulGH0UtOJr3onwqaoTOe20okDcB/+G/vwdLgCtOuOdae2bHeADGBCL5CePBcdU4wAD6JoFgM55P4DgSh1oQAJAoIA3br70WzDABALggK94gAAl+fAuggAAIfkECAUAAAAsAAAAAGQAZAAABv5AkXBILBqPyCEAIEFoGB5AckqtWq/YIgBC4Dgil8phgciaz+gs0wMeZEIPeKghSdvvaSZjIn7E/XIPE3V2UniHUwAEBhd/gSGPGIZoEhQWhIiZIgAaIB+AjqFwDwqTZwQDAwEWppppEAZuj6KOBwx3Hm8PFQEarmhMFBVxs8V/AwSFCqEHBhCtv0kADAkHkLTGcBdleRizGQUC0NFaHBVv2dhxCZhnABHGCw7t5EoIEx3X6dkT41cAH7KBI+BP07QG+tQZG9OglB0JGdTFGSCu3iYBA/Zh61AggAEGCApa0aBw1AIO9DIBUFBB46wGGDyEFImFgcsQHTCkxCPBQP4+haE6JGAggSYRoyIMuPSTIQAEgwasAZWzYAIrMyspMNBQFAlGUNlGBdjpTsFPjQcSeCBLRYKYCg02gBDwTAuDAGdLZtCJB4CAllMfLKA7aQkWgMQeZDhQwWpXIWs+BW6GFMm0jFMPTHiqBIKAeVgmhHLUgENIIhJAdACLbYFDNAgQTl3gDLWCRh+4WeHw6FGGBgZ0b1IQcCoyYBNcVoiihMCFA3EqWKiJtgGDwhZk77vA+TCHvMYaXN0kwcECUB0oYCG5D1KHXpMQbGD9DQRbI9NapnuwgSBkAgXoIsdelbkFlCMXMCcEAhHQFxQFBSbgYCAN+LcJA9qFEkCBFP44kMAA9M0ygAKYyDdVA8JNQYFU2UhniF+y0MLOYUxoIEAEEaXTgQGTaHDBPhk4YAUEgGWzgIIr5ZPQgAl0dwYEGMRoDAiTWCBZOr4kYgBQlEEmAIuiLBCAhXkgAMIwxuxFiF8L7BfBfRpgZswDb0LGAJq0NCAAW1tUNk0BIeLEwYsYCEjLkdKAYGhvFXCjyJXMTHDaURIQoMCHF0z3DwQOrBbIHxVUJIIEjaRT5xEEBJTNAeoJAQGgxXQwqBYaKBAgIA1keQQCZHp5nhyjzKErA562puBRCuSITZOQbVnMSa3A8gE6cnBnGQcfgKCBKX7h+UgCLyqazoa7euDFBv5SCqYgAW2KIiu3DFyADigPgGsZCCH8BuFRAvzKjABDoKJQByketQQTBAiAwQagjdqIKAfMOkR5YF7TwQAGJCEBPHBkEIFwAHhQLLAfZAlAVOlwgNWLZhXTDxH3fHNBADIVFFtiISSwrRIGKDugA4YgAGI2DVRmBAIFFIMizDgG8ptpRm0MJn+6jjqfiMlsopQ6HWSNFQWLPtCBqKNOYGhOdVmxhQL+5ruBbgBYMHQokizocygEnkHqNy9D5sCnA9yCho+G2tussh1UMEAELzaYTQH3IUEAfQt4LQID0I2SqdFGaJCAoa+JcE8CATjAgQUIpO1X2HIMUPUV+IpCrv6rPx4zXpkPw/GB10xA8JgRAhuTnhkQyPlHBnBjwGIH1yGCwK9wnGqFBMmpA3QWIs+yAREaFJcvBq58+ccCgqtdqDrSVwGAA3eHoAD3Hn5wQAEF3yGBhHJQWVO7StePxP2zqJzBmkCAnRmEAfyrgJOo0L0WWY4KN/tW5H4BAcfFwQNYINWEHlC+KtxpFvqzyBEUwKIJHMYDAUhAAQawmj88oFVWWBEzQidC7gHmAR/gHBN45QEFYGACF3DNP9jWgQ9EICYEmGA0JNAAYhCsEEtoghLJk0TO1UMACbhAA3JVwy4eYoeX8KIYx0jGMpoRK76z4hm7KIENXCABGGjeGv6VUBQ/1VGNqDmPYj6wQC/WiAEKCEAQeXQFC3TEAArwAAEQ8Lt/4E9sHRRjduYFh7pZgQHzKmIBIuAROVrBWYq53hhXcrcM0HAK7PKDC+Ewvz4mwQLAKoArLbK+MGHwCrFZEqg0Nb1PwCE9eJxCE54RrQ28xH9HqCA2QnCA9/1jS3/YwCzTUJ4GJMABGCCTBcBTryluIgBzesDsrGCla2QAhogglhOPpZRHHMCSauuZOqSZwQCAggyZgEATR8EsyHjvDx3g5RUQqA7ySYMJPfKlHy5gQDtAwGxyqEAHBZAnb7pqn6KAAyEhs4WEGSAAIIAb2Mz5sTtAQFx/CKEIXv5VDJVhxYLFKMCLGPGBCrQwSIWBx/Ho94oAROQPF8BEkmZxAGSqqGIuzICmVkIvPwQuPvNxYaga2ZbsAEJ3lvPRLMSJx+cVAxIhmB0CFArWENDzPwEKhGaMqoS7jOw3nqReRhcgUDUkYE4hwOcmlPep7xUGQEGpqxYQQIEIzE0OHziWCATATXFatAgU/apiQpjL0XTgWABgkCNKlgQA2ZQ1D7hAr9hVjAEslQEG8ABX7jgFCPxTROXLniPgECpTSABb65hmocAqVfscxUq0yIDEKpuvxWEgkQRI21EcsEHHQqZ6vaEItwgQATGR5R2s6cAGPCkE4BYjqM2amjmhdf4EBhhvFpcdwlhbw91ReeB2WjCmHDRDF+Cliyq8jFNv0EM21EB0mXSAjAeGMZq8OoNbBSFV4hJAgZkUoRL6gdhrsJuOs5a3baN5QMOGOts4ZCAB8BWmBhyMHwQEIHMQ2/AydHTKo9hTsjjZ16gw4CA/fODAPFFAE9XhFMjIDSgWRoIGUNxXP1RoYrHrDSsvQKJCOIDIouin6DbAOhdGEgk928fShCABDDSWtxVwAAOIqYYXB7fHXL4rUPoGwVIp5G0T4wCGg/WH+YGAA+2Vxm5T3B257uM4auuXRkrqpddutWN6jSE3XYOJk7YvKBJTm6IkohidKcFzSAWWlbFggf4iKWZzQ4CATw8k5Sto8Ku8FW1hMPLoR7jUCmMdBcaEioBHpiM3aJBbOOWgJ1NowAEEzkYAsKBPwQTAk3G7GlDSCwwKjKzDo8hJOyRgAS+ro2j/mMCx27GS+wYXnu6ISmAeUIAw0pECQYTyArxpmEs3KDBNeawRylPlADqgV5sgAAbAsICIdOB1/0BALAIDh80gws9TyVdDoLEEBADSABhg6xEkoGNrLCXIeHhovVdZL5EczGiZrfhS4JAAiZshNVDeLxyEhIgaoVBJBFeMwaNxMm4WA53uyKwFODCBBvwk5vlCMzlWMufRPNAMBEhATTNALaBLu4aKyF1GHzDN3ZJYHGcxb4BiRSjqZ4+iAvJWgk9HzoySl7ESO36ETAuhU7LDYURVpyUC2BcKlT7JmECXeRLn2N0ItNB9wRRBqsjOkTyvUQIeSMB5AGYHdbokLU3m+8Q/A/DDUGCD840ABagqeYMF/mQSuRgGthX4zqvNzPMdwAVAQBTTe1ECgMpAB6wJAgVYQLmuh7oAEok6MnsxCAAh+QQIBQAAACwAAAAAZABkAAAG/kCRcEgsGo/IIQAgASSf0Kh0Sk0CIBYFKKKper/gMNMzuVQOoYwlLFyy32IJY3LOhB73DoENMEQ8EE5wg08ABAYXeHh3jANdYgF3HxgWEoSXShoOHw+KjJ4NCGwSEYoZFRMWgphvEBicnZ+yDwUQbBAbi3enAY+sXkwUFZ6zshG2YRINd8QPBxwIq79QAAwJdp7ZsxOWYQgDi4qmBQLS00cAHODa7IwhIOZVCAuyswsO3edFAAgTHSEA27F74CAeFQIZsrHLUICAQVbVlgUsJvCBgYdSLMQS2EhAvmkAPAygSBJPhwsB1rBhUHJWBw4fMQFQUKEluwYYPETDKEUA/kd2HTDEHCTBwIGfnx4sSMCgyaV+C7DZTBMAGaE+R6cqjaAqmQan+xCAGIZUUYChYGZmRXogQjkjPIsIKJAggIBA+xgEWFuyEz44AAQMs/mgAgW8bQw5UNnzX4YDqCpJk+ABls0OF/kwGMl2giglEAQkCkGBCgN6nRR9gDYZRIeyHRTETYKgQdkFBj5CUDD6DgcqFmqGC9jAwGchEhR8mDqAcRUAE6ZW8JDP0AW+IRxQ0cA56Z0DDRisAmDBts0LVqek+/fzQlcRTDAs2MhsAhUEy5sBBNih1yoEufyUwVlUVCMcSQw51AYBBUhFzAazIZffLPRd4MF/15B0xwIU/kQoggQJ0HeTgvAxIFE7H3gIAAjr1JPNAArkg0ACLYVwgS9QUIAGRxWI14ZgDs5SAVpISKABBREkVFJQq2hgXkUYSAHBge0scGEbCqBBUQYLBHCcF64MoKRCDwyYDwFPbrlHIRj8dEBm8AnAXjH9vSeGWIMRExAGggQ2pzZ3RECkEBpMOJCgbTBApSwNeGTFcww0mJQisQkiQZscWWkFCEESU8FnhliWzQGemcOEBgq49QRYoDnw2n7iTGfpaAIhegQBy5F0QGlC4ELRS6aiKukDvKJTgAF2/jifniF84AsDr1Z55T4GdPpJAt0A0GYxC3BgDgQGfGAHIx4kIUFC/h/4R0Rg3aWGRwR9clpSAPFIcEFJFbwlAgGLmgTTugxcgI04zhWBwEYXdLiuAPO5g8cBAgxBADgkLfClEAz8BC9ytH7yzGQO7DjRAx3gWIQGA6fh2boeRMvMA862YVRJv607AUcdqKQWOxNI0493B1wAwrS3vupOAiTCV62eGRQkxDccNSDNihc0sMBr2SQgCAIF3PQlAkm6k0EDrFFDwQZj3rHBmr2G6N0Aa2q7JNvwSQABBAhoQAADHlDAgQNxC9BpB/p+OIGDQSEWxRUKYJ3aBscZ0i4zfD6dNjEZVA7MvdlkwI0SDow8AANhaCBwQA9orcTM2VSADAClUETL/qBJWBBkCAvQzYDID7jn4REaoL1IBt4OURugvAZmLR4f0D0FCBTSOwQEnN/R3O/oIJDIIs0roQA7BQgysUC7gpknIxlEjoHIOWNPG2qd2PrhBx8UEAEIBijg44exD+Q0FS3TxgaI4KRPRAkTAljLAkinBDegY2kUkd/iHHC5EChACRwY2AUuBgdSuAMe2/kbCCaQgAI0YACv2SAVQGQPtklAeBkgnvuo4QHUhGAA6VncEiRgt7wRwALJgoL2nNGBDixgAARqAwIs4AEFUIB2yXAbHiKmjycgwAAO4AAFPGABDSiuiuvKEiPsA0YkLGGG05gHM1JUxja+gR8WYAAF/mTjxjra8Y54zKMe98jHPvrxj4AMJB8NQQANIOCLgqwjAGj0mAYULJFuNMDw/mdHJuAtjgKgAAfdaIFP1AIkejvk1ML1gQFUYAEL+AADpXAFBBySh2d0nzIY0QGFscICnIjhp6a3AVgBJBQrDMAALrCBCAQABBjInwBMVghJMmMDOexgADzxOSFY4ChZg+K6pvkyU3SAklGwAMVCcIALYgJatCycAUZGTs2xEoIjwwM02wCBHwpAARgIQAKsIoEASMVil4DAk1JnFQC0iFKrnMJpxOGwBbYhOs1wZ6HEcSM0EgEC3MTDdIjAklk0QJsGS5NC4JSxWcSsDRTYSAYi/rDJFYKAd39BTgGKUbPnlEIgG+iGBCjmiaaNJwKfYEhLowCBCfAOPd7jy8OG+gQdlSRuGZzF6P4TIGbkC5GFMJGDujcE07FDeh8SAAMsUMhXwtJnNvzq1gzFiHkuqKrf+cNsJGCBANADfR8gGgAy+okFMIYDijjAES9gvwkYID2L1BBA4bO+WWRuPAwSEhWzhySeMiKv0kigNsyEnGWIKDUfiIlPZOfTp3lWFgsoHD8yJA5zHsFeB+jUBZK2r37dUGdZGo4uQnDAi7KVGNcTQsu4RbQPqSMWGSgXEhCg1MI4ALG4LEYG4FQb2TmDmQBwgIiSkkToUKgwxTVEBOaj/oYkoOwTHdjA/oZQnnZ8Umm8cwdAJMhe2wZ2lQXUj6yKIIcAVICZEqOUW6KJq3ZUgDEaOJ9CIGYQCdwsatlSVJUOu48rYIQAC4iAJh04BAkIg50PoyN8+kcRtx4hOH2J6Xq2lIAgUoPD69LAXtpxABV/b0m2RMfNcNwnTFHkA4e1aId5s12zvE6cfcmpECuYjQa4kK/NCFqMxOCBAvyDJNgyHto0lFwpVKslwEQOBv7EDFhVwAEMwOoTogNiRhygKrz0Jc8iNMS+JOA4EuBAwxTisQvkhgoPpkiN+RmJn8BNPQzjSBpYqgQBsJVCNwQwEkJHkQVMuVec+glmfrei/uVdK3IaSEB8m3FgKnBAur6bnj9tkuUqQKBrfbFR0gBAAXFVLKFQEIBU/6XEXiIlzF7gl6Lx0ChpEMABB0VvsaLAEkV0ab3k8XVLOqDcL8zEZQNRilCIQFcHpBUgzwDOw0DQlDAmux2P5QMHlOqiNLjnI4y7wAKwmW4p1KZsSiDATW3C2TdIgIJauQdt4UMADETghOAsREz4YQDL/mQC0QSDg8sCkLEZx1T8YMDAV0iBBowayxEPQ1E9zdAyDYDCV2Gcx7Vyhzv/QgIvHfbLeAsYAGhAAK5i+cMgfo4+kLkdjKgpGHZTBpJvCc76yLN9i7HsL3hg6W7aNhjJsz2bmFTb2gow+kAaUFwwYvTb7MD1cyRJcUpBDo8ebgDJnfccDOj8AScPeRn5od0iH0DSrAxdWWIYgY3jkQAJ2DMjdpkMKP+qAOv1I2Ui4LIGyD0KpHBTAu4CSRFAgAyvQqrIcyFot0Sj8olBAAcaUE0wHC8pHRgABr4iZDxamA34edgwQVAJ0JdxRgkAgQIsoGbb/4IfTWh9FYIAADtIRWF0eDVlbEYyMWxPUExORWtwZmdDcDhlU2dhY212VkhnenZIZ1U0SkR3Z1JsYzdMTXBZbjlGRUo1eTRzbngr\')"></div>' +
      '</div></div>');
  } else {
    $('.addFilePreload').remove();
  }
}

function loadFile(type){
  var id = $('#inputUploadFileName').val().trim();
  placeholder('add')
  $.post(
    "index.php",
    {
      action: 'add-file',
      type: type,
      id: id
    },
    function( data ) {
      data = parse(data);
      if(data['code'] == 200){
        view(data);
        closeUpdateProcessBox();
        res(0,0,'.file-' + id);
        setTimeout(function(){openFile(id + '.csv', 0, 0, 0)}, 1000);
      } else {
        view(data);
      }

      placeholder('remove');
      return data['code'] == 200;
    }
  );
}

function getOpenId() {
  var query = window.location.href;
  var vars = query.match(/\/([0-9]+)$/);
  if (vars){
    return vars[1];
  }
  return null;
}

$(document).ready(function() {

  /* ----------------------------------- Auto load file ----------------------------------- */

  var openId = getOpenId();

  if(openId) {
    addFile(openId);
  }

  /* ----------------------------------- Download ----------------------------------- */

  $('#startButton').bind(
    'click',
    function(){
      start();
      $('#startButton')
        .text('Wait ...')
        .bind('click', function(){})
        .addClass('disabled');
    }
  );

  /* ----------------------------------- Settings ----------------------------------- */

  $('#settingsClose').bind(
    'click',
    function(){
      $('#settingsBox').animate({opacity:0}, 500, function(){
        $('#settingsBox').hide();
      });
      $('#settingsContent').animate({marginTop:'3%'}, 500);
    }
  );

  $('.settButt').bind(
    'click',
    function(element){
      if(element.target.innerText != ''){
        $('.settButt').removeClass('active');
        element.target.className = 'settButt active';
      }
    }
  );

  $('#PACK').bind(
    'mousemove',
    function(element){
      $('#PACK_VALUE').val(element.target.value > 0 ? element.target.value : 1);
    }
  ).bind(
    'dblclick',
    function(element){
      element.target.value = 25000;
      $('#PACK_VALUE').val(25000);
    }
  );

  $('#settSave').bind(
    'click',
    function(){
      var param =              {};
      param.CSV_FOLDER =       "'" + $('input[name=CSV_FOLDER]').val() + "'";
      param.DOWNLOAD_FOLDER =  "'" + $('input[name=DOWNLOAD_FOLDER]').val() + "'";
      param.PACK =             parseInt($('input[name=PACK]').val());
      param.PROCESS =          parseInt($('input[name=PROCESS]').val());
      param.PROXY_ACTIVE =     $('input[name=PROXY_ACTIVE]').is(':checked') ? 'true' : 'false';
      param.PROXY_SERVER =     "'" + $('input[name=PROXY_SERVER]').val() + "'";
      param.PROXY_AUTH =       "'" + $('input[name=PROXY_AUTH]').val() + "'";
      param.API_PATH =         "'" + $('input[name=API_PATH]').val() + "'";
      param.API_KEY =          "'" + $('input[name=API_KEY]').val() + "'";
      param.THEME =            "'" + $('.settButt.active').val() + "'";

      $.post(
        "index.php",
        {
          action: 'settings-save',
          param: JSON.stringify(param)
        },
        function( data ) {
          data = parse(data);
          if(data['code'] == 200){
            res(0,1,0);
          } else {
            view(data);
          }
          return data['code'] == 200;
        }
      );
    }
  );

  $('#PROCESS').bind(
    'mousemove',
    function(element){
      $('#PROCESS_VALUE').val(element.target.value > 0 ? element.target.value : 1);
    }
  ).bind(
    'dblclick',
    function(element){
      element.target.value = 10;
      $('#PROCESS_VALUE').val(10);
    }
  );

  $('.settBottomLine button').bind(
    'dblclick',
    function(){
      $('.settBottomLine button').html("<b class=\"icon-loop2\"></b> Updating ...");
      $.post(
        "index.php",
        {
          action: 'update'
        },
        function( data ) {
          data = parse(data);
          if(data['code'] == 200){
            res(0,1,0);
          } else {
            view(data);
            $('.settBottomLine button').html("<b class=\"icon-cloud-download\"></b> Force update");
          }
          return data['code'] == 200;
        }
      );
    }
  );

  /* ----------------------------------- Update ----------------------------------- */

  $('#updateClose').bind(
    'click',
    function(){
      closeUpdateBox();
    }
  );

  $('#updateProcessClose').bind(
    'click',
    function(){
      closeUpdateProcessBox();
    }
  );

  setTimeout(function(){
    $.post(
      "./",
      {
        action: 'check-update'
      },
      function( data ) {
        data = parse(data);
        if(data['code'] == 200){
          $('#updateProcessBox').show().animate({opacity:1}, 500);
          $('#updateProcessContent').animate({marginTop:'5%'}, 500);
          $.post(
            "./",
            {
              action: 'update'
            },
            function( data ) {
              data = parse(data);
              if(data['code'] == 200){
                setTimeout(function(){
                  window.location = './';
                }, 3000);
              } else {
                view(data);
                $('#updateProcessBox').animate({opacity:0}, 500, function(){
                  $('#updateProcessBox').hide();
                });
                $('#updateProcessContent').animate({marginTop:'3%'}, 500);
              }
              return data['code'] == 200;
            }
          );
        }
        return data['code'] == 200;
      }
    );
  }, 1000);

  /* ----------------------------------- News ----------------------------------- */

  $.post(
    "./",
    {
      action: 'get-news'
    },
    function( data ) {
      data = parse(data);
      var subContent = $('.subContent');
      if(data['code'] == 200){
        subContent.html(data['data']);
        $('ul.tabs li').click(function(){
          var tab_id = $(this).attr('data-tab');
          $('ul.tabs li').removeClass('current');
          $('.tab-content').removeClass('current');
          $(this).addClass('current');
          $("#"+tab_id).addClass('current');
          $("#"+tab_id+" article:nth-child(2n)").css({'margin': '0 -200px 0 200px', 'opacity': 0});
          $("#"+tab_id+" article:nth-child(2n-1)").css({'margin': '0 200px 0 -200px', 'opacity': 0});
          var i = 0;
          $("#"+tab_id+" .news-item").each(function(){
            $(this).delay(1+i).animate({opacity:1,marginLeft:0,marginRight:0}, 700, 'easeInOutBack');
            i += 170;
          });
        });
        $('input[name=NEWS_ACTIVE]').bind(
          'change',
          function (item) {
            $.post("./", {action: 'news-active'});
            if (item.target.checked) {
              subContent.show(250).removeClass('hide');
              $('.tabs .tab-link').addClass('active');
              setTimeout(function () {$('#footer').removeClass('absolute');}, 250);
            } else {
              subContent.hide(250).addClass('hide');
              $('.tabs .tab-link').removeClass('active');
              $('#footer').addClass('absolute');
            }
          }
        );
        $('.tab-link').bind(
          'click',
          function (item) {
            $.post("./", {action: 'news-tab', tab: item.target.innerText});
          }
        );
      }
      if (!subContent.hasClass('hide')) {
        subContent.css({'display': 'block', 'opacity': 0});
        setTimeout(function () {
          subContent.animate({opacity:1}, 300);
          var i = 170;
          $('.current .news-item').each(function(){
            $(this).delay(1+i).animate({opacity:1,marginLeft:0,marginRight:0}, 700, 'easeInOutBack');
            i += 170;
          });
        }, 2000);
      }
      $('#footer').animate({opacity:1}, 300);
      return data['code'] == 200;
    }
  );

});