<?php

$UPDATE_INFO = array(
  '4.8.4' => array(
    'Fix presta images',
    'Add remove magneticone proxy server from source url',
  ),
  '4.8.3' => array(
    'Fix gz files',
  ),
  '4.8.2' => array(
    'Fix url without schema',
    'Skip nop uploaded images',
  ),
  '4.8.1' => array(
    'Fix footer',
  ),
  '4.8.0' => array(
    'Change update server to bitbucket repo',
  ),
  '4.7.6' => array(
    'Fix source url port detected',
  ),
  '4.7.5' => array(
    'Fix source url GET params query error',
  ),
  '4.7.4' => array(
    'Fix news chanel',
  ),
  '4.7.3' => array(
    'Fix fucking spaces',
  ),
  '4.7.2' => array(
    'Fix url encode',
  ),
  '4.7.1' => array(
    'Fix url special chars encode',
  ),
  '4.7.0' => array(
    'Optimize download duplicate file',
    'Fix block site wish max request',
  ),
  '4.6.0' => array(
    'Optimize download big csv file process',
    'Fix file size detect',
    'Fix open file row format',
  ),
  '4.5.12' => array(
    'Fix load file animate process',
  ),
  '4.5.11' => array(
    'Add skip ssl certificate verification for curl request',
  ),
  '4.5.10' => array(
    'Add placeholder for open file process',
  ),
  '4.5.9' => array(
    'Remove christmas theme',
  ),
  '4.5.8' => array(
    'Christmas theme',
  ),
  '4.5.7' => array(
    'Fix urldecode function for plus symbol',
  ),
  '4.5.6' => array(
    'Fix presta images',
  ),
  '4.5.5' => array(
    'Change news chanel',
  ),
  '4.5.4' => array(
    'Change news chanel',
  ),
  '4.5.3' => array(
    'Add file upload preloader',
  ),
  '4.5.2' => array(
    'Fix download file from app',
    'Add author blog to news',
  ),
  '4.5.1' => array(
    'Add to TeamCity',
  ),
  '4.4.11' => array(
    'Small fix image download',
  ),
  '4.4.10' => array(
    'Fix png to jpg convert',
  ),
  '4.4.9' => array(
    'Fix design, video and scroll bar',
    'Fix error comma in url',
  ),
  '4.4.8' => array(
    'Fix design',
    'Add news custom filter',
  ),
  '4.4.7' => array(
    'Fix design',
    'Fix image download',
  ),
  '4.4.6' => array(
    'Fix design',
  ),
  '4.4.5' => array(
    'Delete Хабрахабр chanel',
    'Fix error',
  ),
  '4.4.4' => array(
    'Add validate image content',
    'Fix error',
  ),
  '4.4.3' => array(
    'Add new chanel',
    'News tab now saved',
    'Fix footer',
  ),
  '4.4.2' => array(
    'Fix news',
  ),
  '4.4.1' => array(
    'Add news tab',
    'Add news active button',
  ),
  '4.4.0' => array(
    'Add news block',
    'Change logo color',
  ),
  '4.3.1' => array(
    'Rebase project to private account',
    'Change git project url and email',
  ),
  '4.3.0' => array(
    'Optimize download image',
    'Add new amazing scroll bar',
    'Add url decode to target url',
    'Add jpeg to convert image',
    'Split option PrestaShop image to two small option',
  ),
  '4.2.9' => array(
    'Add check API key',
  ),
  '4.2.8' => array(
    'Fix error connect db',
    'Remove action fileInfo',
  ),
  '4.2.7' => array(
    'Disable ssl check',
    'Remove replace https to http for source url',
    'Small fix css',
  ),
  '4.2.6' => array(
    'Fix https image download',
    'Add set permission auto download csv file',
  ),
  '4.2.5' => array(
    'Fix update process',
  ),
  '4.2.4' => array(
    'Fix delete empty image',
  ),
  '4.2.3' => array(
    'Small fix download image',
  ),
  '4.2.2' => array(
    'Fast options PrestaShop images now convert all images to .jpg',
  ),
  '4.2.1' => array(
    'Add workflow link',
    'Add auto open file after download files copy queue',
    'Add quick file download method, write migration in http path and press enter (need force update)',
  ),
  '4.2.0' => array(
    'Fast options PrestaShop images now working',
    'Add animations',
  ),
  '4.1.0' => array(
    'Fix image download process',
    'Add auto download files_copy_queue.csv',
    'Add Api settings',
  ),
  '4.0.0 Alpha' => array(
    'Convert CSS to LESS structure',
    'Optimize color theme',
    'Theme new change all site color',
    'Add animation for more elements',
    'New download bar and grid',
    'Add fast settings (using proxy, other extension)',
    'New amazing favicon',
    'Add update informer',
    'Grid size information (download image info and free disc space)',
    'And of course create 100500 new bug, because life is pain',
  ),
  '3.2.6' => array(
    'Small fix update process',
  ),
  '3.2.5' => array(
    'Change check version method to version_compare()',
    'Add update log',
  ),
);
